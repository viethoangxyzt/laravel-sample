<?php

namespace App\Service;

use App\Jobs\SendMailPassword;
use App\Models\SocialAccount;
use App\Repositories\Department\DepartmentRepositoryInterface;
use App\Repositories\Project\ProjectRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;



class UserService
{
   public function __construct(DepartmentRepositoryInterface $departmentRepositoryInterface, UserRepositoryInterface $userRepositoryInterface, ProjectRepositoryInterface $projectRepositoryInterface)
   {
      $this->departmentRepositoryInterface = $departmentRepositoryInterface;
      $this->userRepositoryInterface = $userRepositoryInterface;
      $this->projectRepositoryInterface = $projectRepositoryInterface;
   }

   /**
    * get all user
    * 
    * @return Repository
    */
   public function list(int $paginate)
   {
      return $this->userRepositoryInterface->getList($paginate);
   }

   //get all user in department
   public function getUserInDepartment(int $id, int $paginate)
   {
      $users = $this->userRepositoryInterface->getUserInDepartment($id, $paginate);
      return $users;
   }

   /**
    * get manager by departmentId
    * @param $id
    */
   public function getManagerByDepartmentId(int $id)
   {
      return $this->userRepositoryInterface->getManagerByDepartmentId($id);
   }

   /**
    * process img
    * @return mixed
    */
   public function processImg($data)
   {
      $photoName = time() . '.' . $data['image']->extension();
      $img = Image::make($data['image']);
      $img->resize(300, 300, function ($const) {
         $const->aspectRatio();
      })->save(public_path('uploads') . '/' . $photoName);
      return $photoName;
   }

   /**
    * Create new user
    * @param $data
    * @return mixed
    */
   public function create(array $data)
   {
      //img process
      isset($data['image']) ? $data['image'] = $this->processImg($data) : '';
      $data['password'] = Str::random(10);
      SendMailPassword::dispatch($data);
      $data['password'] = Hash::make($data['password']);
      $data['first_login'] = config('common.IS_FIRST_LOGIN');
      return $this->userRepositoryInterface->create($data);
   }

   /**
    * find User by ID
    */
   public function find($id)
   {
      return $this->userRepositoryInterface->find($id);
   }

   /**
    * Update department by id
    * @return mixed
    */
   public function update(int $id, $data)
   {
      //img process
      isset($data['image']) ? $data['image'] = $this->processImg($data) : '';
      return $this->userRepositoryInterface->update($id, $data);
   }

   /**
    * Reset password
    * @param $request
    * @return mixed
    */
   public function resetPassword($id)
   {
      $user = $this->userRepositoryInterface->find($id);
      $random_password = Str::random(config('common.LENGTH_PASSWORD_DEFAULT'));
      $user['password'] = Hash::make($random_password);
      $user['first_login'] = 1;
      $user->save();
      //send mail password default to user
      $data = [
         'email' => $user->email,
         'name' => $user->name,
         'subject' => '[RIKKEI - RESET MẬT KHẨU]',
         'password' => $random_password
      ];
      dispatch(new \App\Jobs\SendMailPassword($data));
      Artisan::call('queue:work --stop-when-empty', []);
      return $user;
   }

   /**
    * Export user to excel
    * @param $request
    * @return mixed
    */

   public function exportUserExcel($request)
   {
      if ($request->has('name')) {
         $data = $this->userRepositoryInterface->search($request->name, Config::get('common.paginate.limit_0'));
      } else {
         $data = $this->userRepositoryInterface->sort($request, Config::get('common.paginate.limit_0'));
      }
      return $data;
   }

   /**
    * Search user
    * @return mixed
    */
   public function search(string $keyword)
   {
      return $this->userRepositoryInterface->search($keyword, Config::get('common.paginate.limit_3'));
   }

   /*
    * Sort user
    * @param $request
    * @return mixed
    */
   public function sort($request)
   {
      return $this->userRepositoryInterface->sort($request, Config::get('common.paginate.limit_3'));
   }

   /*
    * Delete user by id
    * @param $id
    * @return mixed
    */
   public function delete(int $id)
   {
      $user =  $this->userRepositoryInterface->find($id);
      // delete image
      $avatar_name = public_path('uploads') . '/' . $user->image;
      if (File::exists($avatar_name)) {
         File::delete($avatar_name);
      }
      $provider = SocialAccount::where('user_id', $user->id)->delete();
      return $this->userRepositoryInterface->delete($id);
   }

   /**
    * Get User By Email
    */
   public function getByEmail(string $email)
   {
      return $this->userRepositoryInterface->getByEmail($email);
   }

   /**
    * get project of user
    */
   public function getProjectOfUser($userId)
   {
      return $this->projectRepositoryInterface->getProjectOfUser($userId);
   }

   /**
    * get list Users Api
    */
   public function getListUsersApi()
   {
      return $this->userRepositoryInterface->getListUsersApi();
   }

   /**
    * get list Users Api no paginate
    */
   public function getFullListUsers()
   {
      return $this->userRepositoryInterface->getFullList();
   }

   /**
    * api get user by id
    */

   public function getUserByIdAPi(int $id)
   {
      return $this->userRepositoryInterface->getUserByIdAPi($id);
   }

   /**
    * list user by keyword search 
    */
   public function searchApi(string $keyword)
   {
      return $this->userRepositoryInterface->listUserSeacrhApi($keyword);
   }

   /**
    * list user by conditional sort 
    */
   public function sortApi($department_id, $status, $sort)
   {
      return $this->userRepositoryInterface->listUserSortApi($department_id, $status, $sort);
   }

   /**
    * store data Api
    */
   public function storeApi(array $data)
   {
      $image = $data['image'];
      if ($image) {
         $photoName = $photoName = time() . '.' . $image->extension();
         $img = Image::make($image);
         $img->resize(300, 300, function ($const) {
            $const->aspectRatio();
         })->save(public_path('uploads') . '/' . $photoName);
         $data['image'] = $photoName;
      }
      return $this->userRepositoryInterface->create($data);
   }

   /**
    * send mail reset password for user
    */
   public function sendMailChangePassword($email)
   {
      $token = Str::random(64);
      DB::table('password_resets')->insert([
         'email' => $email,
         'token' => $token,
         'created_at' => Carbon::now()
      ]);

      Mail::send('emails.forgotPassword', ['token' => $token], function ($messages) use ($email) {
         $messages->to($email);
         $messages->subject('Reset Password');
      });
   }
}
