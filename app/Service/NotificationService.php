<?php

namespace App\Service;

use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;

class NotificationService
{
    public function __construct(NotificationRepositoryInterface $notificationReposirotyInterface, UserRepositoryInterface $userRepositoryInterface)
    {
        $this->notificationRepositoryInterface = $notificationReposirotyInterface;
        $this->userRepositoryInterface = $userRepositoryInterface;
    }

    /**
     * get list notifications show index
     */
    public function list(int $paginate)
    {
        return $this->notificationRepositoryInterface->getList($paginate);
    }

    /**
     * create new notification
     */
    public function create(array $data)
    {
        $this->notificationRepositoryInterface->create($data);
    }

    /**
     * get notification of user
     */

    public function getNotificationUser(int $userId)
    {
        $notifications = $this->notificationRepositoryInterface->getNotificationUser($userId);
        return $notifications;
    }

    /**
     * find notification by id
     */
    public function find(int $id)
    {
        return $this->notificationRepositoryInterface->find($id);
    }

    /**
     * update notification
     */
    public function update(int $id, array $data)
    {
        return $this->notificationRepositoryInterface->update($id, $data);
    }

    /**
     * delete notification
     */

    public function delete(int $id)
    {
        $this->notificationRepositoryInterface->delete($id);
        $this->notificationRepositoryInterface->deleteUserNotification($id);
    }

    /**
     * get user by notification id
     */
    public function getListUserByNotificationId(int $id)
    {
        return $this->notificationRepositoryInterface->getListUserByNotificationId($id);
    }
}
