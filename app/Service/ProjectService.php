<?php

namespace App\Service;

use App\Models\UserInProject;
use App\Repositories\Department\DepartmentRepositoryInterface;
use App\Repositories\Project\ProjectRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\UserInProject\UserInProjectRepositoryInterface;

class ProjectService
{
    public function __construct(ProjectRepositoryInterface $projectReposirotyInterface, UserRepositoryInterface $userRepositoryInterface, DepartmentRepositoryInterface $departmentRepositoryInterface, UserInProjectRepositoryInterface $userInProjectRepositoryInterface)
    {
        $this->projectRepositoryInterface = $projectReposirotyInterface;
        $this->userRepositoryInterface = $userRepositoryInterface;
        $this->departmentRepositoryInterface = $departmentRepositoryInterface;
        $this->userInProjectRepositoryInterface = $userInProjectRepositoryInterface;
    }

    /**
     * get list project show index
     */
    public function list(int $paginate)
    {
        return $this->projectRepositoryInterface->getList($paginate);
    }

    /**
     * create new project
     */
    public function create(array $data)
    {
        return $this->projectRepositoryInterface->create($data);
    }

    /**
     * find project by id
     */
    public function find($id)
    {
        return $this->projectRepositoryInterface->find($id);
    }

    /**
     * update project
     */
    public function update($id, $data)
    {
        return $this->projectRepositoryInterface->update($id, $data);
    }

    /**
     * delete project
     */
    public function delete($id)
    {
        return $this->projectRepositoryInterface->delete($id);
    }

    /**
     * add user to project
     */
    public function addUserProject($data, $id)
    {
        $data['project_id'] = $id;
        return $this->userInProjectRepositoryInterface->create($data);
    }

    /**
     * update user in project
     */
    public function updateUser($projectId, $userId, $data)
    {
        $userProject = UserInProject::where('user_id', $userId)->where('project_id', $projectId)->get();
        $update = $userProject[0];
        return $this->userInProjectRepositoryInterface->update($update->id, $data);
    }

    /**
     * delete User from project
     */
    public function deleteUser($projectId, $userId)
    {
        $userProject = UserInProject::where('user_id', $userId)->where('project_id', $projectId)->get();
        $update = $userProject[0];
        return $this->userInProjectRepositoryInterface->delete($update->id);
    }
}
