<?php

namespace App\Service;

use App\Models\User;
use App\Models\SocialAccount;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Contracts\Provider;

class SocialAccountService
{
    public function createOrGetUser(Provider $provider)
    {  
        $providerUser = $provider->user();
        $providerName = class_basename($provider);
        $account = SocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            return $account->user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName,
                'user_id'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'password' =>Hash::make('123123'),
                    'is_admin' => config('common.IS_NOT_ADMIN'),
                    'departments_id' => '1',
                    'is_manager' => config('common.IS_MEMBER'),
                    'start_work' => '2020-01-01',
                    'birthday' =>'2000-02-02',
                    'status' => config('common.IS_WORK'),
                    'first_login' => config('common.IS_FIRST_LOGIN'),
                ]);
            }
            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }
}
