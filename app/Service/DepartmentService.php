<?php

namespace App\Service;

use App\Repositories\Department\DepartmentRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;

class DepartmentService
{


    public function __construct(DepartmentRepositoryInterface $departmentReposirotyInterface, UserRepositoryInterface $userRepositoryInterface)
    {
        $this->departmentRepositoryInterface = $departmentReposirotyInterface;
        $this->userRepositoryInterface = $userRepositoryInterface;
    }

    // get list departments show index
    public function list(int $paginate)
    {
        $departments = $this->departmentRepositoryInterface->getList($paginate);
        $departments->load('users');
        return $departments;
    }

    /**
     * find department by id 
     */    
    public function find($id)
    {
       $department = $this->departmentRepositoryInterface->find($id);
       return $department;
    }

    public function delete($id)
    {
        
        return $this->departmentRepositoryInterface->delete($id);
    }

    /**
     * Create new department
     * @return mixed
     */
    public function create($data)
    {
        return $this->departmentRepositoryInterface->create($data);
    }
   
    /**
     * Update department by id
     * @return mixed
     */
    public function update( $id, $data)
    {
        //lấy quản lý hiện tại
        $manager =  $this->userRepositoryInterface->getManagerByDepartmentId($id);
        if (isset($data['manager'])) {
            //nếu phòng đã có quản lý mà thay đổi thì xóa quản lý cũ
            if ($manager != null && $manager->id != $data['manager']) {
                $manager->is_manager = config('common.IS_MEMBER');
                $manager->save();
            }
            $user =  $this->userRepositoryInterface->find($data['manager']);
            $user->is_manager = config('common.IS_MANAGER');
            $user->save();
        }
        $department['name'] = $data['name'];
        $department['description'] = $data['description'];
        return $this->departmentRepositoryInterface->update($id, $department);
    }

    /**
     * add multi user to department
     */
    public function addUser($department_id, $users_id)
    {
        foreach($users_id as $user_id)
        {
            $data['departments_id'] = $department_id;
            $this->userRepositoryInterface->update($user_id, $data);
        } 
    }
}
