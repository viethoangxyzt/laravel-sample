<?php

namespace App\Repositories\Department;


use App\Models\Department;
use App\Repositories\BaseRepository\BaseRepository;

class DepartmentRepository extends BaseRepository implements DepartmentRepositoryInterface
{
    //lấy model tương ứng
    public function getModel()
    {
        return Department::class;
    }
    
    public function getList(int $paginate)
    {
        return $this->model->with('users')->paginate($paginate);
    }

    /**
     * get One
     * 
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    
    public function find($id)
    {
        $data = $this->model->with('users')->find($id);
        return $data;
    }
    
    
}
