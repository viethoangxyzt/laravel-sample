<?php

namespace App\Repositories\UserInProject;

use App\Models\Project;
use App\Models\UserInProject;
use App\Repositories\BaseRepository\BaseRepository;
use Illuminate\Support\Facades\DB;

//use Your Model

/**
 * Class UserInProjectRepository.
 */
class UserInProjectRepository extends BaseRepository implements UserInProjectRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function getModel()
    {
        return UserInProject::class;
    }

     
}
