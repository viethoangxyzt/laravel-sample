<?php

namespace App\Repositories\BaseRepository;

use Illuminate\Support\Facades\Auth;

abstract class BaseRepository implements BaseRepositoryInterface
{
    protected $model;
    function __construct()
    {
        $this->setModel();
    }
    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->getModel()
        );
    }

    /**
     * getAll
     *
     * @return void
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * store
     *
     * @param  mixed $data
     * @return void
     */
    public function create($data)
    {
        $this->model->create($data);
    }

    /**
     * get One
     * 
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function find($id)
    {
        $data = $this->model->find($id);
        return $data;
    }

    /**
     *  Update
     * 
     * @param int $id
     * @param array $data
     * @return bool|mixed
     */
    public function update($id, $data)
    {
        return $this->find($id)->update($data);
    }

    /**
     * Delete
     *
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $result = $this->find($id);
        if ($result) {
            $result->delete();

            return true;
        }

        return false;
    }

    /**
     * Search user by keyword
     * @param $keyword
     * @return mixed
     */
    public function search(string $keyword, int $paginate)
    {
        if (Auth::user()->is_manager == config('common.IS_MANAGER') && Auth::user()->is_admin == config('common.IS_NOT_ADMIN')) {
            return $this->model->where('name', 'like', '%' . $keyword . '%')->where('departments_id', Auth::user()->departments_id)->paginate($paginate);
        }
        return $this->model->where('name', 'like', '%' . $keyword . '%')->paginate($paginate)->withQueryString();
    }
}
