<?php

namespace App\Repositories\Notification;

use App\Models\Notification;
use App\Models\UserNotification;
use App\Repositories\BaseRepository\BaseRepository;
use App\Repositories\Notification\NotificationRepositoryInterface;
use Illuminate\Support\Facades\DB;

//use Your Model

/**
 * Class NotificationRepository.
 */
class NotificationRepository extends BaseRepository implements NotificationRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function getModel()
    {
        return Notification::class;
    }

    /**
     * get list project
     */
    public function getList(int $paginate)
    {
        return $this->model->paginate($paginate);
    }

    /**
     * get notification of user
     */
    public function getNotificationUser(int $userId)
    {
        $notifications = DB::table('notifications')
            ->join('user_notifications', 'notifications.id', '=', 'user_notifications.notification_id')
            ->where('user_notifications.user_id', '=', $userId)
            ->select('notifications.*', 'user_notifications.read', 'user_notifications.id as notiUserId')
            ->get();
        return $notifications;
    }

    /**
     * get user by notification id
     */
    public function getListUserByNotificationId(int $id)
    {
        $userNotification = DB::table('user_notifications')
            ->join('users', 'user_notifications.user_id', '=', 'users.id')
            ->where('user_notifications.notification_id', '=', $id)
            ->select('user_notifications.*', 'users.name as user_name')
            ->get();
        return $userNotification;
    }

    /**
     * delete user of notification by noti_id
     */
    public function deleteUserNotification(int $id)
    {
        UserNotification::where('notification_id', $id)->delete();
    }
}
