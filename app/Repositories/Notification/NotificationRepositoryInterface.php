<?php

namespace App\Repositories\Notification;

use App\Repositories\BaseRepository\BaseRepositoryInterface;

interface NotificationRepositoryInterface extends BaseRepositoryInterface
{
    public function getList(int $paginate);
    public function getNotificationUser(int $userId);
    public function getListUserByNotificationId(int $id);
    public function deleteUserNotification(int $id);
}
