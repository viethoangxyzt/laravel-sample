<?php

namespace App\Repositories\Project;

use App\Models\Project;
use App\Repositories\BaseRepository\BaseRepository;
use App\Repositories\Project\ProjectRepositoryInterface;
use Illuminate\Support\Facades\DB;

//use Your Model

/**
 * Class ProjectRepository.
 */
class ProjectRepository extends BaseRepository implements ProjectRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function getModel()
    {
       return Project::class;
    }

    /**
     * get list project
     */
    public function getList(int $paginate)
    {
        return $this->model->paginate($paginate);
    }

    /**
     * get project of User by userId
     */

    public function getProjectOfUser(int $userId)
    {
        $projects = DB::table('projects')
        ->join('user_in_projects', 'projects.id','=','user_in_projects.project_id')
        ->where('user_in_projects.user_id','=', $userId)
        ->select('projects.*', 'user_in_projects.start_user_date', 'user_in_projects.end_user_date')
        ->get();
        return $projects;
    }
}
