<?php

namespace App\Repositories\Project;
use App\Repositories\BaseRepository\BaseRepositoryInterface;

interface ProjectRepositoryInterface extends BaseRepositoryInterface
{
    public function getList(int $paginate);
    public function getProjectOfUser(int $userId);
}