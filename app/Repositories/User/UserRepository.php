<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseRepository\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\User as ResourcesUser;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    //lấy model tương ứng
    public function getModel()
    {
        return User::class;
    }

    public function getUserLogin()
    {
        return $this->model->where('name', 'admin')->first();
    }

    /**
     * get all User
     */
    public function getList(int $paginate)
    {
        return $this->model->paginate($paginate);
    }

    /**
     * get full list not paginate
     */
    public function getFullList()
    {
        return $this->model->get();
    }

    /**
     * Get user in department
     */
    public function getUserInDepartment(int $id, int $paginate)
    {
        return $this->model->where('departments_id', $id)->paginate($paginate);
    }

    /**
     * Get manager by department id
     * @param $id
     * @return mixed
     */
    public function getManagerByDepartmentId(int $id)
    {
        return $this->model->where('departments_id', $id)->where('is_manager', config('common.IS_MANAGER'))->first();
    }

    /**
     * Get list user by department id
     * @param $id
     * @return mixed
     */
    public function getListUserByDepartmentId(int $id, int $paginate)
    {
        return $this->model->where('departments_id', $id)->paginate($paginate);
    }

    /**
     * Sort user
     * @param $id
     * @param $data
     * @return mixed
     */
    public function sort($request, $paginate)
    {
        $users = User::where('deleted_at', null);
        if ($request->department_id) {
            $users->where('departments_id', $request->department_id);
        }
        if ($request->sort) {
            switch ($request->sort) {
                case "sort_by_id":
                    $users->orderby('id', 'asc');
                    break;
                case "birthday_asc":
                    $users->orderby('birthday', 'asc');
                    break;
                case "birthday_desc":
                    $users->orderby('birthday', 'desc');
                    break;
                case "start_work_asc":
                    $users->orderby('start_work', 'asc');
                    break;
                case "start_work_desc":
                    $users->orderby('start_work', 'desc');
                    break;
                default:
                    $users;
                    break;
            };
        }

        if ($request->status != 'all') {
            $users->ByStatus($request->status);
        }
        if (Auth::user()->is_manager == config('common.IS_MANAGER') && Auth::user()->is_admin == config('common.IS_NOT_ADMIN')) {
            $users->where('department_id', Auth::user()->department_id);
        }
        $users = $users->paginate($paginate)->withQueryString();
        return $users;
    }

    /**
     * Get User by Email
     */
    public function getByEmail(string $email)
    {
        return $this->model->where('email', $email)->first();
    }

    /**
     * Create API list User
     */

    public function getListUsersApi()
    {
        $users = DB::table('users')
            ->join('departments', 'users.departments_id', '=', 'departments.id')
            ->where('users.deleted_at', '=', NULL)
            ->select('users.*', 'departments.name as department_name')
            ->paginate(config('common.paginate.limit_3'));

        foreach ($users as $user) {
            $user->url_edit = route('user.api.edit', $user->id);
            $user->url_delete = route('users.destroy', $user->id);
            $user->url_resetpass = route('reset', $user->id);
            $user->url_profile = route('profile', $user->id);
        }
        return $users;
    }

    /**
     * get user by id api
     */
    public function getUserByIdApi(int $id)
    {
        $user = User::findOrFail($id);
        return new ResourcesUser($user);
    }

    /**
     * api search 
     */
    public function listUserSeacrhApi(string $keyword)
    {
        $users = DB::table('users')
            ->join('departments', 'users.departments_id', '=', 'departments.id')
            ->where('users.deleted_at', '=', NULL)
            ->where('users.name', 'LIKE', '%' . $keyword . '%')
            ->select('users.*', 'departments.name as department_name')
            ->paginate(config('common.paginate.limit_3'));
        foreach ($users as $user) {
            $user->url_edit = route('user.api.edit', $user->id);
            $user->url_delete = route('users.destroy', $user->id);
            $user->url_resetpass = route('reset', $user->id);
            $user->url_profile = route('profile', $user->id);
        }
        return $users;
    }

    /**
     * list user by conditional sort
     */

    public function listUserSortApi($department_id, $status, $sort)
    {
        $users = DB::table('users')
            ->join('departments', 'users.departments_id', '=', 'departments.id')
            ->where('users.deleted_at', '=', NULL)
            ->select('users.*', 'departments.name as department_name');
        if ($department_id != 0) {
            $users = $users->where('departments_id', $department_id);
        }

        if ($status == 'all') {
            $users = $users;
        } else {
            $users = $users->where('status', $status);
        }

        if ($sort) {
            switch ($sort) {
                case "sort_by_id":
                    $users->orderBy('id');
                    break;
                case "birthday_asc":
                    $users->orderBy('birthday');
                    break;
                case "birthday_desc":
                    $users->orderBy('birthday', 'desc');
                    break;
                case "start_work_asc":
                    $users->orderBy('start_work');
                    break;
                case "start_work_desc":
                    $users->orderBy('start_work', 'desc');
                    break;
                default:
                    $users;
                    break;
            };
        }

        $users = $users->paginate(3);

        foreach ($users as $user) {
            $user->url_edit = route('user.api.edit', $user->id);
            $user->url_delete = route('users.destroy', $user->id);
            $user->url_resetpass = route('reset', $user->id);
            $user->url_profile = route('profile', $user->id);
        }
        return $users;
    }
}
