<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepository\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    public function getUserLogin();
    public function getList(int $paginate);
    public function getUserInDepartment(int $id, int $paginate);
    public function getListUserByDepartmentId(int $id, int $paginate);
    public function sort($request, int $paginate);
    public function getByEmail(string $email);
    public function getListUsersApi();
    public function getUserByIdApi(int $id);
    public function listUserSeacrhApi(string $keyword);
    public function listUserSortApi($department_id, $status, $sort);
}
