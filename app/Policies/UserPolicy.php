<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

     /**
     * Display view when user's role is Admin
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAdmin(User $user)
    {
        if($user->is_admin == config('common.IS_ADMIN')){
            return true;
        }
        return false;
    }

    /**
     * Display view when user's role is Admin, User
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        if($user->is_admin == config('common.IS_ADMIN') || $user->is_manager == config('common.IS_MANAGER')){
            return true;
        }
        return false;
    }

    /**
     * Display view when user's role is User
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewUser(User $user){
        if($user->is_admin != config('common.IS_ADMIN') && $user->is_manager != config('common.IS_MANAGER')){
            return true;
        }
        return false;
    }
}
