<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepartmentRequest;
use App\Http\Requests\AddUserToDepartmentRequest;
use App\Service\DepartmentService;
use App\Service\UserService;

class DepartmentController extends Controller
{
    protected $departmentService;
    protected $userService;

    public function __construct(DepartmentService $deparmentService, UserService $userService)
    {
        $this->departmentService = $deparmentService;
        $this->userService = $userService;
    }

    // Hiển thị danh sách phòng ban
    public function list()
    {
        $departments =  $this->departmentService->list(config('common.paginate.limit_3'));
        return view('backend.departments.list_department', compact("departments"));
    }

    // gọi bảng tạo phòng ban
    public function create()
    {
        $users = $this->userService->list(config('common.paginate.limit_0'));
        return view("backend/departments/add_department",  compact("users"));
    }

    //Lưu dữ liệu từ form add 
    public function store(DepartmentRequest $request)
    {
        $data = $request->safe()->only(['name', 'description']);
        $department = $this->departmentService->create($data);
        return redirect()->route('department.index')->with('success', __('message.create_success'));
    }

    // Gọi trang sửa thông tin phòng ban
    public function edit($id)
    {
        $department = $this->departmentService->find($id);
        $users = $this->userService->getUserInDepartment($id, config('common.paginate.limit_0'));
        return view("backend.departments.edit_department", compact("department", "users"));
    }

    // Cập nhật thông tin phòng ban
    public function update(DepartmentRequest $request, $id)
    {
        $data = $request->all();
        $this->departmentService->update($id, $data);
        return redirect()->route('department.index')->with("success", __('message.update_success'));
    }

    /**
     * delete department
     */
    public function delete($id)
    {
        $department = $this->departmentService->find($id);
        if (count($department->users) > 0) {
            return redirect()->back()->with("error", __('message.delete_error'));
        } else {
            $department = $this->departmentService->delete($id);
            return redirect()->back()->with("success", __('message.delete_success'));
        }
    }

    /**
     * form add user to department
     */
    public function formAddUser()
    {
        $users = $this->userService->list(config('common.paginate.limit_0'));
        $departments =  $this->departmentService->list(config('common.paginate.limit_0'));
        return view('backend.departments.add_user', compact('users', 'departments'));
    }

    /**
     * add user to department
     */
    public function addUser(AddUserToDepartmentRequest $request)
    {
        $department_id = $request->department;
        $filter = $department_id;
        $department = $this->departmentService->find($department_id);
        $users_id = $request->users;
        $this->departmentService->addUser($department_id, $users_id);
        return redirect()->route('user.index', ['filter' => $filter])
            ->with("success", __('message.add_users_to_department', ['department' => $department->name]));
    }
}
