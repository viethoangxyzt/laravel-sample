<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Service\SocialAccountService;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite as Socialite;

class SocialAuthController extends Controller
{
    
    protected $socialAccountService;

    public function __construct(SocialAccountService $socialAccountService)
    {
        $this->socialAccountService = $socialAccountService;
        
    }
    
    /**
     * redirect to provider
     */
     
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * handle provider callback
     */
    public function handleProviderCallback($provider)
    {   
        $user = $this->socialAccountService->createOrGetUser(Socialite::driver($provider));
        Auth::login($user);
        if(Auth::user()->is_admin == config('common.IS_ADMIN') || Auth::user()->is_manager == config('common.IS_MANAGER'))
        {
            return redirect()->route('user.index');
        } elseif(Auth::user()->first_login == config('common.IS_FIRST_LOGIN')) {
            return redirect()->route('formRegister', Auth::user()->id);
        } else {
            return redirect()->Route('profile', Auth::user()->id);
        }    
    }
}
