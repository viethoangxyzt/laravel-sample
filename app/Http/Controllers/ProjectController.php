<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\Http\Requests\UserProjectRequest;
use App\Models\UserInProject;
use App\Service\DepartmentService;
use App\Service\ProjectService;
use App\Service\UserService;

class ProjectController extends Controller
{

    protected $departmentService;
    protected $userService;
    protected $projectService;

    public function __construct(DepartmentService $deparmentService, UserService $userService, ProjectService $projectService)
    {
        $this->departmentService = $deparmentService;
        $this->userService = $userService;
        $this->projectService = $projectService;
    }

    /**
     * show list project
     */
    public function index()
    {
        $projects = $this->projectService->list(config('common.paginate.limit_3'));
        return view('backend.projects.index', compact('projects'));
    }

    /**
     * show form add project
     */

    public function create()
    {
        return view('backend.projects.add');
    }

    /**
     * Create new project
     */
    public function store(ProjectRequest $request)
    {
        $data = $request->all();
        $project = $this->projectService->create($data);
        return redirect()->route('project.index')->with('success', __('message.create_success'));
    }

    /**
     * form edit project
     */
    public function edit($id)
    {
        $project = $this->projectService->find($id);
        return view('backend.projects.edit', compact('project'));
    }

    /**
     * update project
     */
    public function update(ProjectRequest $request, $id)
    {
        $data = $request->all();
        $project = $this->projectService->update($id, $data);
        return redirect(route('project.index'))->with('success', __('message.update_success'));
    }

    /**
     * delete project
     */
    public function delete($id)
    {
        $this->projectService->delete($id);
        return redirect(route('project.index'))->with('success', __('message.delete_success'));
    }

    /**
     * Detail project 
     */
    public function detail($id)
    {
        $project = $this->projectService->find($id);
        return view('backend.projects.detail', compact('project'));
    }

    /**
     * 
     */
    public function formAddUser($id)
    {
        $project = $this->projectService->find($id);
        $users = $this->userService->list(config('common.paginate.limit_0'));
        return view('backend.projects.add_user', compact('project', 'users'));
    }

    /**
     * add user to project
     */
    public function addUser(UserProjectRequest $request, $id)
    {
        $data = $request->all();
        $this->projectService->addUserProject($data, $id);
        return redirect(route('project.detail', $id))->with("success", __('message.create_success'));
    }

    /**
     * get form edit user information in project
     */
    public function editUser($projectId, $userId)
    {
        $project = $this->projectService->find($projectId);
        $user = $this->userService->find($userId);
        $userProject = UserInProject::where('user_id', $userId)->where('project_id', $projectId)->get();
        return view('backend.projects.edit_user', compact('project', 'user', 'userProject'));
    }

    /**
     * update user information in project
     */
    public function updateUser(UserProjectRequest $request, $projectId, $userId)
    {
        $data = $request->all();
        $project = $this->projectService->find($projectId);
        if ($data['start_user_date'] < $project->start_date || $data['end_user_date'] > $project->end_date) {
            return redirect()->back()->with('error', __('message.date_user_project'));
        } else {
            $this->projectService->updateUser($projectId, $userId, $data);
            return redirect()->route('project.detail', $projectId)->with("success", __('message.update_success'));
        }
    }

    /**
     * delete user from project
     */
    public function deleteUser($projectId, $userId)
    {
        $this->projectService->deleteUser($projectId, $userId);
        return redirect()->route('project.detail', $projectId)->with("success", __('message.delete_success'));
    }

    /**
     * show list project of user
     */
    public function listProjectUser($userId)
    {
        $projects = $this->userService->getProjectOfUser($userId);
        return view('backend.projects.list_project_of_user', compact('projects'));
    }

    /**
     * show list project of user in department
     */
    public function listProjectDepartment($id)
    {
        $manager = $this->userService->find($id);
        $projects = $this->projectService->list(config('common.paginate.limit_0'));
        $projectDepartments = array();
        foreach ($projects as $project) {
            foreach ($project->users as $user) {
                if ($user->departments_id  == $manager->departments_id) {
                    $projectDepartments[] = $project;
                }
            }
        }
        $projectDepartments = array_unique($projectDepartments);
        return view('backend.projects.list_project_of_department', compact('projectDepartments'));
    }
}
