<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use App\Models\UsersJoinProject;
use App\Service\DepartmentService;
use App\Service\UserService;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    protected $userService;
    protected $departmentService;

    public function __construct(UserService $userService, DepartmentService $departmentService)
    {
        $this->userService = $userService;
        $this->departmentService = $departmentService;
    }

    public function index()
    {
        return view("backend/index");
    }
    public function logout()
    {
        Auth::logout();
        return redirect("/login");
    }

    /**
     * calculate the percentage of users by department
     */
    public function handleChart()
    {
        $total_user = User::count();
        $departments = $this->departmentService->list(config('common.paginate.limit_0'));

        // get data for pie-chart
        $dataPoints = [];
        foreach ($departments as $department) {
            $users_in_department = User::where('departments_id', $department->id)->count();
            $dataPoints[] = [
                'name' => $department->name,
                'y' => floatval($users_in_department / $total_user)
            ];
        }

        // get data for column chart
        $months = [];
        $dataBar = [];
        $dataBar[0] = [
            'name' => "Total User",
        ];
        $dataBar[1] = [
            'name' => "User Join Project"
        ];
        $usersJoinProjects = UsersJoinProject::orderBy('id', 'DESC')->limit(6)->get();
        foreach ($usersJoinProjects as $usersJoinProject) {
            $month = date("m", strtotime($usersJoinProject['date']));
            $year = date("Y", strtotime($usersJoinProject['date']));
            // get month for Ox
            $months[] = $month . '/' . $year;

            $dataBar[0]['data'][] = $usersJoinProject['total_users'];
            $dataBar[1]['data'][] =  $usersJoinProject['users_join_project'];
        }

        return view('backend.admin.dashboard', [
            "data" => json_encode($dataPoints),
            "months" => json_encode($months),
            "dataBar" => json_encode($dataBar)
        ]);
    }
}
