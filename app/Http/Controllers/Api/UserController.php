<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as ResourcesUser;
use App\Models\User;
use App\Models\UsersJoinProject;
use App\Service\DepartmentService;
use App\Service\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    protected $userService;
    protected $departmentService;

    public function __construct(UserService $userService, DepartmentService $departmentService)
    {
        $this->userService = $userService;
        $this->departmentService = $departmentService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->userService->getListUsersApi();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        return $this->userService->getFullListUsers();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /**
         * validation input value
         */
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:30',
            'email' => 'required|email|unique:users,email,' . $request->id,
            'image' => 'mimes:jpeg,jpg,png|max:5120',
            'phone' => 'nullable|regex:/^[0-9]{10}$/|unique:users,phone,' . $request->id,
            'birthday' => 'required|before:today|date_format:Y/m/d',
            'start_work' => 'required|before:today|date_format:Y/m/d',
            'departments_id' => 'required|exists:departments,id',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fails',
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors()->toArray(),
            ]);
        }

        $data = $request->all();
        return $this->userService->storeApi($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return $this->userService->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user_fail = $this->userService->find($id);
        if (is_null($user_fail)) {
            return response()->json([
                'messagage' => 'User does not exist'
            ], 404);
        }

        /**
         * validation input value
         */
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:30',
            'email' => 'required|email|unique:users,email,' . $request->id,
            'image' => 'mimes:jpeg,jpg,png|max:5120',
            'phone' => 'nullable|regex:/^[0-9]{10}$/|unique:users,phone,' . $request->id,
            'birthday' => 'required|before:today|date_format:Y/m/d',
            'start_work' => 'required|before:today|date_format:Y/m/d',
            'departments_id' => 'required|exists:departments,id',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fails',
                'message' => $validator->errors()->first(),
                'errors' => $validator->errors()->toArray(),
            ]);
        }

        $data = $request->all();
        $user = $this->userService->update($id, $data);
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->delete()) {
            return new ResourcesUser($user);
        }
    }

    /**
     * Display a listing of the resource by keyword search.
     *
     * @param  string $keyword  
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $params = $request->all();
        return $this->userService->searchApi($params['term']);
    }

    /**
     * Display a listing of the resource by filter condition.
     *
     * @param tinyint $status 
     * @param int $department_id 
     * @param string $sort
     * @return \Illuminate\Http\Response
     */
    public function sortUser($department_id, $status, $sort)
    {
        return $this->userService->sortApi($department_id, $status, $sort);
    }

    /**
     * Get Value Pie Chart
     */
    public function pieChart()
    {
        $total_user = User::count();
        $departments = $this->departmentService->list(config('common.paginate.limit_0'));

        // get data for pie-chart
        $dataPoints = [];
        foreach ($departments as $department) {
            $users_in_department = User::where('departments_id', $department->id)->count();
            $dataPoints[] = [
                'name' => $department->name,
                'y' => floatval($users_in_department / $total_user)
            ];
        }
        return $dataPoints;
    }

    /**
     * Get Value Col Chart
     */
    public function colChart()
    {
        $months = [];
        $dataBar = [];
        $dataBar[0] = [
            'name' => "Total User",
        ];
        $dataBar[1] = [
            'name' => "User Join Project"
        ];
        $usersJoinProjects = UsersJoinProject::orderBy('id', 'DESC')->limit(6)->get();
        foreach ($usersJoinProjects as $usersJoinProject) {
            $month = date("m", strtotime($usersJoinProject['date']));
            $year = date("Y", strtotime($usersJoinProject['date']));
            // get month for Ox
            $months[] = $month . '/' . $year;
            $dataBar[0]['data'][] = $usersJoinProject['total_users'];
            $dataBar[1]['data'][] =  $usersJoinProject['users_join_project'];
        }

        return [
            "months" => $months,
            "dataBar" => $dataBar
        ];
    }
}
