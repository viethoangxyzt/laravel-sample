<?php

namespace App\Http\Controllers;

use App\Exports\ExportUser;
use App\Http\Requests\ExcelRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UserRequest;
use App\Imports\UsersImport;
use App\Models\Department;
use App\Models\User;
use App\Service\DepartmentService;
use App\Service\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PDF;

class UserController extends Controller
{
    protected $userService;
    protected $departmentService;

    public function __construct(UserService $userService, DepartmentService $departmentService)
    {
        $this->userService = $userService;
        $this->departmentService = $departmentService;
    }

    /**
     * Display a listing of the user
     */
    public function list(Request $request)
    {
        if (Auth::user()->is_manager == config('common.IS_MANAGER') && Auth::user()->is_admin == config('common.IS_NOT_ADMIN')) {
            $users = $this->userService->getUserInDepartment(Auth::user()->departments_id, config('common.paginate.limit_3'));
            return view('backend.users.list_user', compact('users'));
        } elseif (Auth::user()->is_admin == config('common.IS_ADMIN')) {
            $params = $request->all();
            $filter = $params['filter'] ?? NULL;
            $departments = $this->departmentService->list(config('common.paginate.limit_0'));
            $users =  $this->userService->list(config('common.paginate.limit_3'));
            return view('backend.users.list_user', compact('users', 'departments', 'filter'));
        }
    }

    /**
     * Show the form for creating a new employee
     */
    public function create()
    {
        $departments = $this->departmentService->list(config('common.paginate.limit_0'));
        return view("backend/users/add_user", compact('departments'));
    }

    /**
     * Store a newly created resource in storege
     */
    public function store(UserRequest $request)
    {
        $data = $request->all();
        $user = $this->userService->create($data);
        return redirect()->route('user.index')->with('success', __('message.create_success'));
    }

    /**
     * get form edit user
     */
    public function edit($id)
    {
        $departments = $this->departmentService->list(config('common.paginate.limit_0'));
        $user = $this->userService->find($id);
        $user->load('department');
        return view("backend/users/edit_user", compact("departments", "user"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request,  $id)
    {
        $data = $request->except(['_token']);
        $this->userService->update($id, $data);
        return redirect(route('user.index'))->with('success', __('message.update_success'));
    }

    // Show detailed employee information
    public function detail($id)
    {
        $user = User::where("id", $id)->get();
        $user->load('department');
        return view("backend/users/detail_user", ["user" => $user[0]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        if (Auth::user()->id == $id) {
            return redirect()->back()->with('error', __('message.delete_yourself'));
        } else {
            $this->userService->delete($id);
            return redirect(route('user.index'))->with('success', __('message.delete_success'));
        }
    }

    // get page update profile
    public function updateProfile($id)
    {
        $user = $this->userService->find($id);
        return view('backend/users/update_profile', compact('user'));
    }

    // update profile
    public function storeProfile(ProfileRequest $request, $id)
    {
        $user = User::find($id);
        $user->phone = $request->phone;
        $user->birthday = $request->birthday;
        if ($request->hasFile('image')) {
            $request->file('image')->move("uploads", $request->file('image')->getClientOriginalName());
            $user->image = $request->file('image')->getClientOriginalName();
        }
        $user->save();
        $request->session()->flash("success", __('message.update_success'));
        return redirect()->route('profile', $user->id);
    }

    // Reset password
    public function resetPassword($id)
    {
        $user = $this->userService->resetPassword($id);
        return redirect()->back()->with('success', __('message.reset_success') . ' ' . $user->email);
    }

    // get page change password
    public function formChangePassword()
    {
        return view("backend/users/form_change_password");
    }

    // change password
    public function changePassword(ResetPasswordRequest $resetPasswordRequest)
    {
        $hashedPassword = Auth::user()->password;
        if (Hash::check($resetPasswordRequest->old_password, $hashedPassword)) {
            if (Hash::check($resetPasswordRequest->password, $hashedPassword)) {
                return redirect()->back()->with('error', __('message.password_duplicate'));
            } else {
                $user = User::find(Auth::user()->id);
                $user->password = Hash::make($resetPasswordRequest->password);
                $user->first_login = config('common.NOT_FIRST_LOGIN');
                $user->save();
                session()->flash('success', __('message.reset_success'));
                return redirect()->route('profile', $user->id);
            }
        } else {
            return redirect()->back()->with('error', __('message.password_fail'));
        }
    }

    /**
     * Search user
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */
    public function search(Request $request)
    {

        if ($request->name == null) {
            $users = $this->userService->list(config('common.paginate.limit_3'));
        } else {
            $users = $this->userService->search($request->name);
        }
        $departments = $this->departmentService->list(config('common.paginate.limit_0'));
        return view('backend.users.list_user', compact('users', 'departments'));
    }

    /**
     *  Sort user
     *  @param Request $request
     *  @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        $users = $this->userService->sort($request);
        $departments = $this->departmentService->list(config('common.paginate.limit_0'));
        return view('backend.users.list_user', compact('users', 'departments'));
    }

    // export file exel
    public function exportUsers(Request $request)
    {
        $data = $this->userService->exportUserExcel($request);
        return Excel::download(new ExportUser($data), 'users.xlsx');
    }

    /**
     * Import excel
     * @return \Illuminate\Http\Response
     *
     */
    public function importExcel()
    {
        return view('backend.excel.importExcel');
    }

    /**
     * Save Import Excel
     */
    public function saveImportExcel(ExcelRequest $request)
    {
        $import = new UsersImport;
        try {
            $import->import($request->file);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            return back()->withFailures($failures);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        $data_return = $import->getDataReturn();
        dd($data_return);
        return redirect(route('user.index'))->with('success', __('message.create_update_excel_success', ['create' => $data_return['count_create'], 'update' => $data_return['count_update']]))->with('list_department_excel', $data_return['department_name']);
    }

    /**
     * form register 
     */
    public function formRegister($id)
    {
        $departments = $this->departmentService->list(config('common.paginate.limit_0'));
        $user = $this->userService->find($id);
        $user->load('department');
        return view('backend.users.register', compact('departments', 'user'));
    }

    /**
     * update infor register
     */
    public function register(UserRequest $request, $id)
    {
        $data = $request->except(['_token']);
        $data['first_login'] = config('common.NOT_FIRST_LOGIN');
        $this->userService->update($id, $data);
        return redirect()->route('profile', $id)->with('success', __('message.update_success'));
    }


    /**
     * create user by ajax + api
     */
    public function createByApi()
    {
        return view('backend.users_api.add');
    }


    /**
     * store user by ajax +api
     */

    public function storeByApi()
    {
        return redirect()->route('users.api.index');
    }

    /**
     * view edit user by ajax + api
     */
    public function editByApi()
    {
        return view('backend.users_api.edit');
    }

    /**
     * export file cv pdf
     */
    public function exportCV($id)
    {
        $user = $this->userService->find($id);
        
        // get type of image
        $image = $user->image;
        $dot_position = strpos($image,'.');
        $length = strlen($image);
        $typeImage = substr($image, $dot_position + 1, $length-1);

        $pdf = PDF::loadView('backend.pdf.cv', compact('user', 'typeImage'));
        return $pdf->download('cv.pdf');
    }
}
