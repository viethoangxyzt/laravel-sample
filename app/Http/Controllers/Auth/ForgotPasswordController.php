<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Service\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

// use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $userService;


    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Form submit Mail to change password
     */
    public function submitMailForm()
    {
        return view('backend.password.forgot');
    }

    /**
     * submit Mail to change password
     */
    public function submitMail(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);

        $this->userService->sendMailChangePassword($request->email);
        return redirect()->back()->with('success', 'Email đã gửi tới tài khoản của bạn');
    }

    /**
     * Show reset password form
     */
    public function showResetPasswordForm($token)
    {
        return view('backend.password.change', ['token' => $token]);
    }

    /**
     * change password
     */
    public function submitResetPasswordForm(Request $request)
    {

        $request->validate([
            'password' => ['required', 'min:6', 'max:30', 'regex:/(?=.*[A-Za-z])(?=.*\d)(?=.*[ !"#$%&\'()*+,-.\/:;<=>?@[\]^_`{|}~])/'],
            'confirm_password' => ['required', 'same:password']
        ]);

        $email = DB::table('password_resets')->where('token', $request->token)->first()->email;
        if (!$email) {
            return redirect()->back->with('error', 'Invalid token');
        }
        User::where('email', $email)->update(['password' => Hash::make($request->password), 'first_login' => config('common.NOT_FIRST_LOGIN')]);
        DB::table('password_resets')->where(['email' => $email])->delete();
        return redirect()->route('login')->with('message', 'Mật khẩu đã được thay đổi');
    }
}
