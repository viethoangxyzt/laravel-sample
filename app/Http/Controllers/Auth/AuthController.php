<?php

namespace App\Http\Controllers\Auth;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    public function getLogin()
    {
        if(Auth::check())
        {
            return redirect("/admin");
        }
        return view("backend/login");
    }

    public function postLogin(LoginRequest $LoginRequest)
    {      
        if(Auth::attempt(['email' => $LoginRequest->email, 'password' => $LoginRequest->password])){

            if(Auth::user()->first_login == config('common.IS_FIRST_LOGIN')) {
                return redirect()->route('formChange');
            }else{
            if(Auth::user()->is_admin != config('common.IS_ADMIN') && Auth::user()->is_manager != config('common.IS_MANAGER')){ 
                return redirect()->route('profile', Auth::user()->id);
            }else{
                return redirect()->route('user.index');
            }
        }

    }
        else{
            return redirect()->back()->withErrors("Tài khoản không hợp lệ!");
        }
    }  
}
