<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    /**
     * Handle an authentication attempt.
     * @param Request $request
     * @return view
     */
    public function login(Request $request)
    {
        if (Auth::check()) {
            return redirect("/admin");
        }
        return view('backend.login');
    }
    /**
     * Handle an authentication attempt.
     * @param Request $request
     * @return redirect
     */
    public function postLogin(LoginRequest $request)
    {
        $login = $request->only('email', 'password');
        if (Auth::attempt($login)) {
            if (Auth::user()->first_login) {
                return redirect()->route('formChange');
            }
            if (Auth::user()->is_admin || Auth::user()->is_manager) {
                return redirect()->route('user.index');
            }
            return redirect()->route('profile', Auth::user()->id);
        } else {
            return redirect()->back()->with('false_login', 'Tài khoản không hợp lệ');
        }
    }

    /**
     * Logout
     * @return redirect
     */
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
