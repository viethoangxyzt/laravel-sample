<?php

namespace App\Http\Controllers;

use App\Http\Requests\NotificationRequest;
use App\Models\Notification;
use App\Models\UserNotification;
use App\Service\UserService;
use App\Service\NotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpseclib3\File\ASN1\Maps\UserNotice;

class NotificationController extends Controller
{

    protected $userService;
    protected $notificationService;

    public function __construct(UserService $userService, NotificationService $notificationService)
    {
        $this->userService = $userService;
        $this->notificationService = $notificationService;
    }

    /**
     * show list project
     */
    public function index()
    {
        if (Auth::user()->is_admin == config('common.IS_ADMIN')) {
            $notifications = $this->notificationService->list(config('common.paginate.limit_0'));
            return view('backend.notifications.list', compact('notifications'));
        } else {
            $notifications = $this->notificationService->getNotificationUser(Auth::user()->id);
            return view('backend.notifications.notification_user', compact('notifications'));
        }
    }


    /**
     * show form add notifications
     */
    public function create()
    {
        $users = $this->userService->list(config('common.paginate.limit_0'));
        return view("backend.notifications.add",  compact("users"));
    }

    /**
     * store notifications
     */
    public function store(NotificationRequest $request)
    {
        switch ($request->input('action')) {
            case 'save':
                $data = $request->all();
                $data['send'] = false;
                $notification = Notification::create($data);
                if (isset($data['users'])) {
                    foreach ($data['users'] as $user_id) {
                        UserNotification::create([
                            "user_id" => $user_id,
                            "notification_id" => $notification->id,
                            "read" => false,
                        ]);
                    }
                }
                return redirect()->route('notifications.index')->with('success', __('message.create_success'));
                break;
            case 'create':
                $data = $request->all();
                $data['send'] = true;
                $notification = Notification::create($data);
                foreach ($data['users'] as $user_id) {
                    UserNotification::create([
                        "user_id" => $user_id,
                        "notification_id" => $notification->id,
                        "read" => false,
                    ]);
                }
                return redirect()->route('notifications.index')->with('success', __('message.create_success'));
                break;
        }
    }

    /**
     * edit form notification
     */
    public function edit($id)
    {
        $notification = $this->notificationService->find($id);
        $users = $this->userService->list(config('common.paginate.limit_0'));
        return view('backend.notifications.edit', compact('users', 'notification'));
    }

    /**
     * update notification
     */
    public function update($id, NotificationRequest $request)
    {
        $notification = $this->notificationService->find($id);
        switch ($request->input('action')) {
            case 'save':
                $data = $request->all();
                $data['send'] = false;
                $this->notificationService->update($id, $data);
                UserNotification::where('notification_id', $id)->delete();
                if (isset($data['users'])) {
                    foreach ($data['users'] as $user_id) {
                        UserNotification::create([
                            "user_id" => $user_id,
                            "notification_id" => $notification->id,
                            "read" => false,
                        ]);
                    }
                }
                return redirect()->route('notifications.index')->with('success', __('message.create_success'));
                break;
            case 'create':
                $data = $request->all();
                $data['send'] = true;
                $this->notificationService->update($id, $data);
                UserNotification::where('notification_id', $id)->delete();
                foreach ($data['users'] as $user_id) {
                    UserNotification::create([
                        "user_id" => $user_id,
                        "notification_id" => $notification->id,
                        "read" => false,
                    ]);
                }
                return redirect()->route('notifications.index')->with('success', __('message.create_success'));
                break;
        }
    }


    /**
     * Mark as read notification
     */
    public function markAsRead(int $id)
    {
        UserNotification::find($id)->update(["read" => true]);
        return redirect()->route('notifications.index');
    }

    /**
     * delete notification
     */
    public function delete(int $id)
    {
        $this->notificationService->delete($id);
        return redirect()->route('notifications.index')->with('success', __('message.delete_success'));
    }
}
