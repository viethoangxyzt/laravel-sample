<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','min:3'],
            'description' => ['nullable', 'max:256'],
            'start_date' => ['required'],
            'end_date' => ['required', 'after:start_date'],
        ];
    }

    public function messages()
    {
        return [
            'required' => __('validation.required'),
            'name.min' => __('validation.name.min'),
            'description.max' => __('validation.max'),
            'end_date.after' => __('validation.user_date'),
        ];
    }
     
}
