<?php

namespace App\Http\Requests;

use App\Models\Project;
use Illuminate\Foundation\Http\FormRequest;

class UserProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {   
        $validator->after(function ($validator) {
            if ($this->input('start_user_date') < $this->input('start_project_date')) {
                $validator->errors()->add('start_user_date', __('message.date_user_project'));
            }
            if($this->input('end_user_date') > $this->input('end_project_date')) {
                $validator->errors()->add('end_user_date', __('message.date_user_project'));
            }

            // Kiểm tra nhân viên đã có trong dự án chưa
            $project = Project::find($this->input('project_id'));
            $userInProject = 0;
            foreach ($project->users as $user) {
                if ($user->id == $this->input('user_id')){
                    $userInProject++;
                    break;
                }
            }

            if($userInProject) {
                $validator->errors()->add('user_id', __('validation.user_in_project'));
            }


            
        });
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_user_date' => 'required',
            'end_user_date' => 'required|after:start_user_date',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {     return [
            'required' => __('validation.required'),
            'end_user_date.after' => __('validation.user_date'),
          ];
    }

}
