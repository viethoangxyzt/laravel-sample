<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3', 'max:30'],
            'email' => ['required', 'email', 'max:255', 'unique:users,email,' .\App\Http\Requests\UserRequest::instance()->id],
            'image' => ['mimes:jpeg,jpg,png', 'max:5120'],
            'phone' => ['nullable', 'regex:/^[0-9]{10}$/', 'unique:users,phone,' .\App\Http\Requests\UserRequest::instance()->id],
            'birthday' => ['required', 'before:today'],
            'start_work' => ['required'],
            'departments_id' => ['required', 'exists:departments,id'],
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => __('validation.required'),
            'email.unique' => __('validation.unique.email'),
            'phone.unique' => __('validation.unique.phone'),
            'name.min' => __('validation.name.min'),
            'name.max' => __('validation.name.max'),
            'email' => __('validation.email'),
            'mimes' => __('validation.mimes'),
            'max' => __('validation.max.string'),
            'image.max' => __('validation.max.size'),
            'phone.regex' => __('validation.regex.phone'),
            'departments_id.required' => __('validation.department'),
            'birthday.before' => __('validation.dob_before'),
            'departments_id.exists' => __('validation.exists'),
        ];
    }
}