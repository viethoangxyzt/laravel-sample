<?php

namespace App\Imports;

use App\Jobs\SendMailPassword;
use App\Models\User;
use App\Rules\PositionExcelRule;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Rules\StatusExcelRule;
use App\Rules\AdminExcelRule;
use Illuminate\Support\Str;
use App\Models\Department;

use Illuminate\Support\Facades\Hash;

class UsersImport implements ToCollection, WithHeadingRow, WithValidation
{
    use Importable;

    public $data_return = ['count_update' => 0, 'count_create' => 0, 'department' => ''];
    public $department_name = [];

    /**
     * @param array $row
     *
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            // xử lý trạng thái làm việc
            (Str::lower($row['status']) == 'đang làm việc') ? $status = config('common.IS_WORK') : $status = config('common.QUIT');
            // xử lý chức vụ
            (Str::lower($row['position']) == 'quản lý') ? $is_manager = config('common.IS_MANAGER') : $is_manager = config('common.IS_MEMBER');
            // xử lý quyền quản trị
            (Str::lower($row['is_admin']) == true) ? $is_admin = config('common.IS_ADMIN') : $is_admin = config('common.IS_NOT_ADMIN');
            // kiểm tra tên phòng đã tồn tại hay chưa
            $exists_department = Department::where('name', $row['department'])->first();
            // nếu tên phòng đã tồn tại thì lấy id còn chưa tồn tại thì tạo mới
            if ($exists_department) {
                $department_id = $exists_department->id;
            } else {
                $department = Department::create([
                    'name' => $row['department'],
                ]);
                $department_id = Department::where('name', $row['department'])->first()->id;
                ($department) ? array_push($this->department_name, $row['department']) : '';
            }
            // kiểm tra email user đã tồn tại hay chưa
            $exists_user = User::where('email', $row['email'])->first();
            // nếu email chưa tồn tại thì tạo mới, tồn tại thì cập nhật
            if ($exists_user) {
                $updated = $exists_user->update([
                    'name' => $row['name'],
                    'email' => $row['email'],
                    'phone' => $row['phone'],
                    'birthday' => date('Y-m-d', strtotime($row['birthday'])),
                    'departments_id' => $department_id,
                    'status' => $status,
                    'is_manager' => $is_manager,
                    'is_admin' => $is_admin,
                    'start_work' => date('Y-m-d', strtotime($row['start_work'])),
                ]);
                ($updated) ? $this->data_return['count_update']++ : $this->data_return['count_update'];
            } else {
                $password = Str::random(config('common.LENGTH_PASSWORD_DEFAULT'));
                $create = User::create([
                    'name' => $row['name'],
                    'email' => $row['email'],
                    'phone' => $row['phone'],
                    'first_login' => config('common.IS_FIRST_LOGIN'),
                    'birthday' => date('Y-m-d', strtotime($row['birthday'])),
                    'password' => Hash::make($password),
                    'start_work' => date('Y-m-d', strtotime($row['start_work'])),
                    'status' => $status,
                    'is_manager' => $is_manager,
                    'is_admin' =>  $is_admin,
                    'departments_id' => $department_id,
                ]);
                if ($create) {
                    $this->data_return['count_create']++;
                    $data = [
                        'email' => $row['email'],
                        'name' => $row['name'],
                        'subject' => '[RIKKEI - MẬT KHẨU CỦA BẠN]',
                        'password' => $password
                    ];
                    SendMailPassword::dispatch($data);
                } else {
                    $this->data_return['count_create'];
                }
            }
        }
    }
    public function rules(): array
    {
        return [
            '*.name' => 'bail|required|min:3|max:30',
            '*.email' => 'bail|required|email',
            '*.phone' => 'nullable|regex:/(0[1-9]{2})[0-9]{7}/',
            '*.birthday' => 'bail|date|required|before:today',
            '*.start_work' => 'bail|required|before:today',
            '*.status' => ['bail', 'required', new StatusExcelRule()],
            '*.position' => ['required', new PositionExcelRule()],
            '*.is_admin' => ['nullable', new AdminExcelRule()],
        ];
    }

    /**
     * @return array
     */
    public function customValidationMessages()
    {
        return [
            'phone.regex' => 'Số điện thoại không đúng định dạng',
        ];
    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

    /**
     * Return total: user create and update,department create
     * @return array
     */

    public function getDataReturn(): array
    {
        $this->data_return['department_name'] = array_unique($this->department_name);
        return $this->data_return;
    }
}
