<?php

namespace App\Imports;

use App\Models\Department;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class DepartmentsImport implements ToCollection, WithHeadingRow, WithValidation
{
    use Importable;

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            Department::create([
                'name' => $row['name'],
                'description' => $row['description'],

            ]);
        }
    }

    public function rules(): array
    {
        return [
            '*.name' => 'required|min:3|max:50|unique:departments,name',
            '*.description' => 'required|max:255',
        ];
    }
}

