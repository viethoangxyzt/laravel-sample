<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    protected $table = 'projects';

    protected $fillable =[
        'name',
        'description',
        'status',
        'start_date',
        'end_date',
    ];

    public function users(){
        return $this->belongsToMany(User::class, 'user_in_projects','project_id','user_id')->withPivot('start_user_date','end_user_date');
    }

}
