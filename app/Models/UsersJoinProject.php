<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersJoinProject extends Model
{
    use HasFactory;
    protected $table = 'users_join_project';

    protected $fillable = [
        'date',
        'total_users',
        'users_join_project',
    ];
}
