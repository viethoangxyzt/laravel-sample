<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'notifications';

    protected $fillable = [
        'title',
        'content',
        'send',
    ];

    /**
     * Get the user associated with the notification.
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_notifications', 'notification_id', 'user_id')->withPivot('read');
    }
}
