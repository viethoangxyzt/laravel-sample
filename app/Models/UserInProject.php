<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInProject extends Model
{
    use HasFactory;
    protected $table = 'user_in_projects';

    protected $fillable = [
        'user_id',
        'project_id',
        'start_user_date',
        'end_user_date',
    ];
}
