<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    const TABLE_NAME = 'users';

    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    protected $dates = ['deleted_at'];
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'is_admin',
        'phone',
        'image',
        'is_manager',
        'departments_id',
        'start_work',
        'status',
        'first_login',
        'password',
        'birthday',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public $sortable = ['birthday', 'start_work'];
    /**
     * Get the department associated with the user.
     */
    public function department()
    {
        return $this->belongsTo(Department::class, "departments_id", "id");
    }

    /**
     * Get the project associated with the user.
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, "user_in_projects", "user_id", "project_id")->withPivot('start_user_date', 'end_user_date');
    }

    /**
     * scopeFilterByWorkStatus
     *
     * @param  mixed $query
     * @return string
     */
    public function scopeByStatus($query, $status)
    {
        return $query->where('status', $status);
    }


    /**
     * Get the notification associated with the user.
     */
    public function notifications()
    {
        return $this->belongsToMany(Notification::class, "user_notifications", "user_id", "notification_id")->withPivot('read');
    }
}
