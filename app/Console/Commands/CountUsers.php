<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\UsersJoinProject;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CountUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monthly:count_users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count users and count users join project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $total_user = User::count();

        // get time 12 month ago
        $range = Carbon::now()->subYear(1);

        //  get total users join project amount 12 months
        $user_in_projects = DB::table('users')
            ->join('user_in_projects', 'users.id', '=', 'user_in_projects.user_id')
            ->where('user_in_projects.start_user_date', '>=', $range)
            ->select('users.id')
            ->distinct()
            ->get()
            ->count();

        $date = Carbon::now();
        UsersJoinProject::create([
            'date' => $date,
            'total_users' => $total_user,
            'users_join_project' => $user_in_projects
        ]);
    }
}
