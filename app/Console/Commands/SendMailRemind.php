<?php

namespace App\Console\Commands;

use App\Jobs\SendMailBirthday;
use App\Models\User;
use Illuminate\Console\Command;


class SendMailRemind extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:mail_send_birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send list birthday user for manager';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::whereMonth('birthday', '=', date('m'))->whereDay('birthday', '=', date('d'))->orderBy('departments_id')->get()->toArray();
           
        if (count($users) > 0) {
            //lấy số lượng user sinh nhật ngày hôm nay theo phòng ban
            $count = User::whereMonth('birthday', '=', date('m'))->whereDay('birthday', '=', date('d'))->selectRaw('count(*) as total, departments_id')->groupBy('departments_id')->orderBy('departments_id')->get()->toArray();
            //lấy manager
           
            $managers = User::where('is_manager', config('common.IS_MANAGER'))->get();
            //split array
            $array_split[0] = array_slice($users, 0, $count[0]['total']);
            $total = count($array_split[0]);
            
            for ($i = 1; $i < count($count); $i++) {
                $array_split[$i] = array_slice($users, $total, $count[$i]['total']);
                $total = $total + $count[$i]['total'];
            }
            
            //gửi mail cho quản lý từng phòng ban
            for ($i = 0; $i < count($array_split); $i++) {
                foreach ($managers as $manager) {
                    if ($manager->departments_id == $array_split[$i][0]['departments_id']) {
                        $data = $array_split[$i];
                        SendMailBirthday::dispatch($manager->email, $data);
                    }
                }
            }
        }
    }
}
