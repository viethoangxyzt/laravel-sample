<?php

namespace App\Jobs;

use App\Mail\MailListUserBirthday;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMailBirthday implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    protected $manager;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $manager, $data)
    {
        $this->manager = $manager;
        $this->data = $data;
       
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new MailListUserBirthday($this->data);
        Mail::to($this->manager)->send($email);
    }
}
