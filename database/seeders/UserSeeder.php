<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123456789'),
            'phone' => '0123456789',
            'is_admin' => true,
            'departments_id' => 1,         
            'is_manager' => true,
            'image' => 'avatar.jpg',
            'start_work' => date('Y-m-d'),
            'birthday' => Carbon::parse('2000-01-01'),
            'status' => 1,  
            'first_login' => 0,

        ]);
    }
}
