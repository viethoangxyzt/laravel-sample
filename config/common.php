<?php
return [
    'IS_WORK' => 1,
    'QUIT' => 0,
    'IS_FIRST_LOGIN' => 1,
    'NOT_FIRST_LOGIN' => 0,
    'IS_ADMIN' => 1,
    'IS_NOT_ADMIN' => 0,
    "IS_MANAGER" => 1,
    "IS_MEMBER" => 0,
    "IS_PENDING" =>1,
    "IS_DEVELOPING" =>2,
    "IS_DONE" => 3,
    "ITEMS_PERPAGE" => 10,
    'DEFAULT_PAGE_SIZE' => 4,
    "LENGTH_PASSWORD_DEFAULT" => 10,
    'paginate'=>[
        "limit_0"=>0,
        "limit_3"=>3,
    ]
];

 define('HEIGHT_RESIZE_IMG', 300);
 define('WIDTH_RESIZE_IMG', 300);
