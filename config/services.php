<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, SparkPost and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    
    'google' => [
        'client_id' => '631168797539-18jesh2h4ftsafm5mf12sms4msf8ii99.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect' => 'http://localhost:49081/google/callback',
    ],

    'facebook' => [
        'client_id' => '1210819659459628',
        'client_secret' => 'c16e171f3e491e19941eeaaf680123ad',
        'redirect' => 'http://localhost:49081/facebook/callback',
    ],
];
