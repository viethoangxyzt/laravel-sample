<?php

return [
    'id' => 'Mã dự án',
    'title' => 'Quản lý dự án',
    'list' => 'Danh sách dự án',
    'name' => 'Tên dự án',
    'description' => 'Mô tả',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày sửa',
    'add' => 'Thêm dự án',
    'edit' => 'Cập nhật dự án',
    'status' => 'Trạng thái',
    'start_date' => 'Ngày bắt đầu',
    'end_date' => 'Ngày kết thúc',
    'is_pending' =>'Đang chờ',
    'is_developing' => 'Đang phát triển',
    'is_done' => 'Đã xong',
    'detail' =>'Thông tin dự án',
    'time' => 'thời gian dự án',
    'start_user_date' => 'Ngày tham gia',
    'end_user_date' => 'Ngày kết thúc',
];