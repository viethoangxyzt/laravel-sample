<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CV</title>
</head>
<body>
    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">{{ __('user.infor') }}</div>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:40px">
                          <div class="col-md-6">

                            <div class="form-group">
                                <label>{{ __('user.image') }} </label>
                                <img id="avatar" class="thumbnail" width="20%" height="150px" src="{{'data:image/'.$typeImage.';base64,'.base64_encode(file_get_contents(public_path('uploads/'.$user->image)))}}">
                                <hr>
                            </div>
                            <div class="form-group">
                                <label>{{ __('user.name') }}</label>
                                <div>{{ $user->name }}</div>
                                <hr>
                            </div>

                            <div class="form-group">
                                <label>{{ __('user.department') }}</label>
                                <div>{{ $user->department->name }}</div>
                                <hr>
                            </div>
                            <div class="form-group">
                                <label>{{ __('user.email') }}</label>
                                <div>{{ $user->email }}</div>
                                <hr>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{ __('user.phone') }}</label>
                                <div>{{ $user->phone }}</div>
                                <hr>
                            </div>

                            <div class="form-group">
                                <label>{{ __('user.birthday') }}</label>
                                <div>{{ date('d-m-Y', strtotime($user->birthday)) }}</div>
                                <hr>
                            </div>

                            <div class="form-group">
                                <label>{{ __('user.start_work') }}</label>
                                <div>{{ date('d-m-Y', strtotime($user->start_work)) }}</div>
                                <hr>
                            </div>
                            @if($user['is_admin'] == config('common.IS_ADMIN'))
                            <div class="form-group">
                                <label>{{ __('user.role') }}</label>
                                <div>Admin</div>
                                <hr>
                            </div>
                            @endif
                            @if($user['is_manager'] == config('common.IS_MANAGER'))
                            <div class="form-group">
                                <label>Quản lí bộ phận</label>
                                <div>{{ $user->department->name}}</div>
                                <hr>
                            </div>
                            @endif
                            <div class="form-group">
                                <label>{{ __('user.status') }}</label>
                                @if($user->status==config('common.IS_WORK')) <div>{{ __('user.is_work') }}</div> @endif
                                @if($user->status==config('common.QUIT')) <div>{{ __('user.quit') }} </div> @endif
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                  
</body>
</html>
