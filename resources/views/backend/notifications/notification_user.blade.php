@extends("backend/master/master")
@section('title')
Quản lí thông báo 
@endsection()
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><svg class="glyph stroked home">
						<use xlink:href="#stroked-home"></use>
					</svg></li>
			<li class="active">Danh sách thông báo</li>
		</ol>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Danh sách thông báo</h1>
		</div>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-xs-12 col-md-12 col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="bootstrap-table">
						<div class="table-responsive">
							@if (session()->has('success'))
							<div class="alert alert-success">
								{{ session()->get('success') }}
							</div>
							@endif
							@if (session()->has('error'))
							<div class="alert alert-danger">
								{{ session()->get('error') }}
							</div>
							@endif	
                            @foreach($notifications as $notification)		
                            <div @if($notification->read == true) class="panel-body alert alert-warning noti" @else class="panel-body alert alert-info noti" @endif>
                                <label class="title">{{ $notification->title }}</label>	
								<button type="button" ddata-title="{{ $notification->title }}" data-content="{{ $notification->content }}" class="btn " data-toggle="modal" data-target="#exampleModalCenter">
									Xem thêm
								</button>	
								<br>
								@if($notification->read == false)			
                                <a href="{{ route('notifications.markAsRead', $notification->notiUserId) }}" class="float-right" data-id="" style="float: right; padding: 0 10px;">
                                    Đánh dấu là đã đọc
								</a>
								@else
								<span style="float: right; color:#30a5ff; padding: 0 10px; ">Đã đọc</span>
								@endif
                                <div class="content" style="white-space: nowrap; width: 800px; overflow: hidden; text-overflow: ellipsis;">{{ $notification->content }}          
                                </div>
                            </div>
							@endforeach  
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!--/.row-->
		</div>
	</div>
</div>
<!--end main-->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	  	<div class="modal-content">
			<div class="modal-header">
		  	<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
		  	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
		  	</button>
			</div>
			<div class="modal-body">
		  	
			</div>
			<div class="modal-footer">
		  	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		  	<button type="button" class="btn btn-primary">Save changes</button>
			</div>
	  	</div>
	</div>
</div>

<script>
	$('#exampleModalCenter').on('show.bs.modal', function(e){
		var title = $(e.relatedTarget).data('title');
		var content = $(e.relatedTarget).data('content');
		$('#exampleModalLongTitle').text(title);
		$('.modal-body').text(content);		
	});
</script>

@endsection
