@extends("backend/master/master")
@section("title")
{{ __('department.add') }}
@endsection
@section("main")
<!--main-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Sửa thông báo</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i>Sửa thông báo</div>
                <div class="panel-body">
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('notifications.update', $notification->id) }}">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Tiêu đề(<span class="text-danger">*</span>)</label>
                                    <input type="text" name="title" class="form-control" value="{{ old('title', $notification->title) }}">
                                    @error('title')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Nôi dung(<span class="text-danger">*</span>)</label>
                                    <input type="text" name="content" class="form-control" value="{{ old('content', $notification->content) }}">
                                    @error('content')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>

                                <div>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                                    <label>Chọn người gửi</label>
                                    <select name="users[]" id="siteID" class="form-control js-example-basic-multiple" style="width:100%" multiple="multiple">
                                    </select>
                                    @error('users')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <input id="notificationId" type="hidden" value="{{ $notification->id }}">
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right" style="margin-top: 20px;">
                                    <button name="action" class="btn btn-success" type="submit" value="create">{{ __('form.send') }}</button>
                                    <button name="action" class="btn btn-warning" type="submit" value="save">{{ __('form.save') }}</button>
                                    <button class="btn btn-danger" type="reset">{{ __('link.cancel') }}</button>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                    <div class="row" style="margin-left: 20px; margin-top: -40px">
                        <a class="btn btn-info" href="{{ route('notifications.index') }}">{{ __('link.back') }}</a>
                    </div>
                </div>

                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>

    <!--/.row-->
</div>

<!--end main-->
<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
            multiple: true,
            ajax: {
                url: "{{ route('api.users.search') }}",       
                data: function(params) {
                    var querryParametters = {
                        term: params.term
                    }
                    return querryParametters;
                },
                dataType: 'json',
                type: 'post',
                processResults: function (data) {
                    return {
                        results: $.map(data.data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            },
        });
        
        var notificationId = $('#notificationId').val();
        let userSelect = $('.js-example-basic-multiple');
        $.ajax({
            url: "{{ route('api.notification.user', $notification->id) }}",
            type: 'GET',
            success: function(response) {
                // loading(false)
                response.forEach(function(element) {
                    let option = new Option(element.user_name, element.user_id, true, true);
                    userSelect.append(option)
                })
                userSelect.trigger('change')
            },
            error: function(error) {
                loading(false)
            }
        });
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" 
integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" 
crossorigin="anonymous" referrerpolicy="no-referrer">
</script>

@endsection
