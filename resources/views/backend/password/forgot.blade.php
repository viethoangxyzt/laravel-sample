<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Quên mật khẩu</title>
	<base href="/backend/" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
</head>

<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Gửi email</div>
				<div class="panel-body">
					@if (session('success'))
					<div class="alert alert-success alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  {{ session('success') }}
					@endif
					<form role="form" method="POST" action="{{ route('submitMail') }}">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="E-mail" name="email" type="text" autofocus="" value="{{old('email')}}">
								@error('email')
								<span class="invalid-feedback text-danger" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
							</div>
							<button type="submit" class="btn btn-primary">Gửi</button>
							@csrf
						</fieldset>
					</form>
								
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->
</body>
</html>
