<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Quên mật khẩu</title>
	<base href="/backend/" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
</head>

<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Đổi Mật khẩu</div>
				<div class="panel-body">
					<form role="form" method="POST" action="{{ route('reset.password.post') }}">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Mật khẩu" name="password" type="password" autofocus="" value="{{old('password')}}">
								@error('password')
								<span class="invalid-feedback text-danger" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
							</div>
                            <div class="form-group">
								<input class="form-control" placeholder="Nhập lại mật khẩu" name="confirm_password" type="password" autofocus="" value="{{old('confirm_password')}}">
								@error('confirm_password')
								<span class="invalid-feedback text-danger" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
							</div>
                            <input type="hidden" name="token" value="{{ $token }}">
							<button type="submit" class="btn btn-primary">Cập nhật</button>
							@csrf
						</fieldset>
					</form>							
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->
</body>
</html>
