<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <ul class="nav menu">
        <li class="text-center" style="font-size : 20px; height: 45px; margin-bottom: 30px 0px; line-height: 45px;"><a href="{{ route('profile', Auth::user()->id) }}">
                @if(Auth::user()->is_admin == config('common.IS_ADMIN'))
                {{ __('user.admin') }}
                @elseif(Auth::user()->is_manager == config('common.IS_MANAGER'))
                {{ __('user.manager') }}
                {{ Auth::user()->department->name }}
                @else
                Nhân viên
                @endif
            </a></li>
        <hr>
        @can('viewAdmin', \App\User::class)
        <li class="{{ Request::is('*/dashboard') ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><svg class="glyph stroked clipboard with paper">
            <use xlink:href="#stroked-clipboard-with-paper" />
        </svg>Thống kê</a></li>
        <li class="{{ Request::is('*/department/*') || Request::is('*/department') ? 'active' : '' }}"><a href="{{ route('department.index') }}"><svg class="glyph stroked clipboard with paper">
                    <use xlink:href="#stroked-clipboard-with-paper" />
                </svg>Quản lí bộ phận</a></li>
        @endcan
        @can('viewAny', \App\User::class)
        <li class="{{ Request::is('*/user/*') || Request::is('*/user') ? 'active' : '' }}"><a href="{{ route('user.index') }}"><svg class="glyph stroked male-user">
                    <use xlink:href="#stroked-male-user"></use>
                </svg> Quản lí nhân viên</a></li>
        <li class="{{ Request::is('*/project/*') || Request::is('*/project') || Request::is('*/listprojectDepartment') ? 'active' : '' }}">
            @if(Auth::user()->is_admin == config('common.IS_ADMIN'))
            <a href="{{ route('project.index') }}">
            @else
            <a href="{{ route('manager.listproject', Auth::user()->id) }}">
            @endif
                <svg class="glyph stroked table">
                    <use xlink:href="#stroked-table"></use>
                </svg> Quản lí dự án</a></li>                
        @endcan
        <li class="{{ Request::is('*/listproject/*') || Request::is('*/listproject') ? 'active' : '' }}"><a href="{{ route('profile.listproject', Auth::user()->id) }}"><svg class="glyph stroked male-user">
            <use xlink:href="#stroked-male-user"></use>
        </svg>Danh sách dự án cá nhân</a></li>
        <li class="{{ Request::is('notifications/*') || Request::is('notifications') ? 'active' : '' }}"><a href="{{ route('notifications.index') }}"><svg class="glyph stroked two messages">
            <use xlink:href="#stroked-two-messages"/>
        </svg>Thông báo</a></li>
    </ul>
</div>