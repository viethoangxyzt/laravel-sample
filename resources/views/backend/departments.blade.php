@extends("backend/master/master")
@section('title')
{{ __('department.title') }}
@endsection()
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><svg class="glyph stroked home">
						<use xlink:href="#stroked-home"></use>
					</svg></li>
			<li class="active">{{ __('department.list') }}</li>
		</ol>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{ __('department.list') }}</h1>
		</div>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-xs-12 col-md-12 col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="bootstrap-table">
						<div class="table-responsive">
							@if (session()->has('success'))
							<div class="alert alert-success">
								{{ session()->get('success') }}
							</div>
							@endif
							@if (session()->has('error'))
							<div class="alert alert-danger">
								{{ session()->get('error') }}
							</div>
							@endif
							<a href="{{ route('department.create') }}" class="btn btn-primary">{{ __('department.add') }}</a>
							<table class="table table-bordered" style="margin-top:20px;">
								<thead>
									<tr class="bg-primary">	
										<th>{{ __('department.name') }}</th>
										<th>{{ __('department.description') }}</th>
										<th>{{ __('department.manager') }}</th>
										<th width='18%'>Tùy chọn</th>
									</tr>
								</thead>
								<tbody id ="load_data">
									
									
								
								</tbody>
							</table>
							<div align='right'>
								
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!--/.row-->
		</div>
	</div>
</div>
<!--end main-->

<!-- javascript -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
	$(document).ready(function(){
		LoadData();
	});

	function LoadData(){
		$.ajax({
			url:'http://localhost:49081/api/departments',
			type:'GET',
			success:function(rs){
				var str="";
				$.each(rs.data, function(i, item){
					str += "<tr>";
					str += "<td>" + item.name + "</td>";
					if(item.description != null){
						str += '<td>' + item.description + '</td>'
					} else {
						str += '<td></td>'
					}
					str += "<td></td>";
				    str += '<td>'
					str += '<a href="{{ route("department.edit",'+ item.id +') }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>'					
					str += '<a href="{{ route("department.delete",'+  item.id + ') }}" onclick="return confirm("Bạn muốn xoá bộ phận {{' + item.name + '}}")"  class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>'
					str += '</td>';
					str += "</tr>";
				});
				$('#load_data').html(str);
			}
		});
	}
</script>
@endsection


									
