@extends("backend/master/master")
@section("title")
{{ __('user.title') }}
@endsection
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><svg class="glyph stroked home">
						<use xlink:href="#stroked-home"></use>
					</svg></li>
			<li class="active">{{ __('user.list') }}</li>
		</ol>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{ __('user.list') }}</h1>
		</div>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-xs-12 col-md-12 col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="bootstrap-table">
						<div class="table-responsive">
							<!-- Thông báo thành công -->
							@if (session()->has('success'))
							<div class="alert alert-success">
								{{ session()->get('success') }}
							</div>
							@endif
							<!-- Thông báo lỗi -->
							@if (session()->has('error'))
							<div class="alert alert-danger">
								{{ session()->get('error') }}
							</div>
							@endif
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							@if (session('list_department_excel') && session('list_department_excel')!=0)
            					<div class="alert alert-success alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                					Danh Sách Phòng tạo mới :
                					<ul class="">
                    					@foreach (session('list_department_excel') as $departerment)
                    						<li class="">{{ $departerment }}</li>
                    					@endforeach
                					</ul>
            					</div>
            				@endif

							@can('viewAdmin', \App\User::class)
							<a href=" {{ route('user.create') }}" class="btn btn-primary">{{ __('user.add') }}</a>
							
							<a href="{{ route('user.createExcel') }}" class="btn btn-primary">Thêm
									Excel</a>
							
							<div style="margin-left: 300px; margin-top: -30px;">
								<!-- sort form -->
								<form type="get" action="{{ route('user.sort') }}">
									<select name="sort" style="height: 30px; margin: 0 20px;">
										<option selected disabled>Sắp xếp</option>
										<option value="sort_by_id" {{ (isset($_GET['sort']) && $_GET['sort'] == 'sort_by_id') ? 'selected' : '' }}>Sắp xếp theo ngày tạo</option>
										<option value="birthday_asc" {{ (isset($_GET['sort']) && $_GET['sort'] == 'birthday_asc') ? 'selected' : '' }}>Ngày sinh tăng dần</option>
										<option value="birthday_desc" {{ (isset($_GET['sort']) && $_GET['sort'] == 'birthday_desc') ? 'selected' : '' }}>Ngày sinh giảm dần</option>
										<option value="start_work_asc" {{ (isset($_GET['sort']) && $_GET['sort'] == 'start_work_asc') ? 'selected' : '' }}>Ngày làm việc tăng dần</option>
										<option value="start_work_desc" {{ (isset($_GET['sort']) && $_GET['sort'] == 'start_work_desc') ? 'selected' : '' }}>Ngày làm việc giảm dần</option>
									</select>
									<select name="department_id" style="height: 30px;  margin: 0 20px;">
										<option value="0">{{ __('user.department') }}</option>
										@foreach($departments as $department)
										<option value="{{ $department['id'] }}" {{ (isset($_GET['department_id']) && $_GET['department_id'] == $department['id']) ? 'selected' : '' }}>{{ $department['name'] }}</option>
										@endforeach
									</select>
									<select name="status" style="height: 30px;  margin: 0 20px;">
										<option value="all" {{ isset($_GET['status']) && $_GET['status'] == 'all' ? 'selected' : '' }}>{{ __('user.status') }}</option>
										<option value="1" {{ isset($_GET['status']) && $_GET['status'] == '1' ? 'selected' : '' }}>{{ __('user.is_work') }}</option>
										<option value="0" {{ isset($_GET['status']) && $_GET['status'] == '0' ? 'selected' : '' }}>{{ __('user.quit') }}</option>
									</select>
									<button type="submit" class="btn btn-success" style="height: 30px; line-height:1;">Lọc</button>
								</form>
								<!-- Sort form -->

							</div>
							<div style="margin-left: 300px; margin-top: 20px;">
								<form action="{{ route('user.search') }}" method="get">
									<input type="text" id="myInput" placeholder="Tìm kiếm theo tên" name="name" value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}" title="Type in a name" style="height: 30px; margin: 0 20px;">
									<button type="submit" class="btn btn-success" style="height: 30px; line-height:1;">Tìm kiếm</button>

								</form>
							</div>
							@endcan
							<table id="table-user" class="table table-bordered" style="margin-top:20px;">
								<thead>
									<tr class="bg-primary">
										<th class="text-center">{{ __('user.image') }}</th>
										<th class="text-center">{{ __('user.name') }}</th>
										<th class="text-center">{{ __('user.birthday') }}</th>
										<th class="text-center">{{ __('user.email') }}</th>
										<th class="text-center">{{ __('user.phone') }}</th>
										<th class="text-center">{{ __('user.department') }}</th>
										<th class="text-center">{{ __('user.start_work') }}</th>
										<th class="text-center">{{ __('user.status') }}</th>
										<th class="text-center">{{ __('user.role') }}</th>
										@can('viewAdmin', \App\User::class)
										<th class="text-center" width='12%'>Tùy chọn</th>
										@endcan
									</tr>
								</thead>
								<tbody>
									@foreach($users as $user)
									<tr>
										<td class="text-center">
											<div class="row">
												<div class="col-md-3"><img src="@if($user->image==""){{ asset('uploads/no_avatar.png') }} @else {{ asset('uploads/'.$user->image) }} @endif" alt="Avatar" width="50px" class="thumbnail"></div>
											</div>
										</td>
										<td class="text-center"><a href="{{ route('profile', $user->id) }}">{{ $user->name }}</a></td>
										<td class="text-center">{{ date('d-m-Y', strtotime($user->birthday)) }}</td>
										<td class="text-center">{{ $user->email }}</td>
										<td class="text-center">{{ $user->phone }}</td>
										<td class="text-center">{{ $user->department->name }}</td>
										<td class="text-center">{{ date('d-m-Y', strtotime($user->start_work)) }}</td>
										<td class="text-center">
											@if($user->status == config('common.IS_WORK'))
											<p class="btn btn-success" href="#" role="button">{{ __('user.is_work') }}</p>
											@else
											<p class="btn btn-danger" href="#" role="button">{{ __('user.quit') }}</p>
											@endif
										</td>
										<td class="text-center">
											@if($user->is_admin == config('common.IS_ADMIN'))
											{{ __('user.admin') }}
											@elseif($user->is_manager == config('common.IS_MANAGER'))
											{{ __('user.manager') }}
											@else
											{{ __('user.user') }}
											@endif
										</td>
										@can('viewAdmin', \App\User::class)
										<td class="text-center">
											@if(Auth::user()->id != $user->id)
											<a href="{{ route('user.edit', $user->id) }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											@else
											<a href="{{ route('profile.update', Auth::user()->id) }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											@endif
											<a href="{{ route('user.delete', $user->id) }}" onclick="return confirm('Bạn muốn xoá {{ $user->name }}?')" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
											<a href="{{ route('reset', $user->id) }}" onclick="return confirm('Cập nhật mật khẩu cho {{ $user->name }}?')" class="btn btn-primary"><i class="fas fa-key" aria-hidden="true"></i></a>
										</td>
										@endcan
									</tr>
									@endforeach
								</tbody>
							</table>
							<div align='right'>
								{{ $users->links("pagination::bootstrap-4 ") }}
							</div>
						</div>
						@can('viewAdmin', \App\User::class)
						<form>
							<a class="btn btn-success" id="export_excel" >{{ __('link.export') }}</a>
						</form>
						@endcan
						<div class="clearfix"></div>
						<input id="filter" value ="{{ isset($filter) ? $filter : ''}}" hidden>
					</div>
				</div>
			</div>
			<!--/.row-->
		</div>
		<!--end main-->

		<!-- javascript -->
		<script>
			//get url query
			var url = window.location.search;
			// regex for "?page=."
			var regex = new RegExp("[?&]page=([^&#]*)");
			//if url has "?page=." then remove it
			(regex.test(url)) ? url = url.replace(regex, ''): '';
			//set url for export
			var url_export_excel = "{{route('user.export')}}" + url;
			//change href for export excel
			$('#export_excel').attr('href', url_export_excel);
		
		</script>
		<script>
			var filter = $('#filter').val();
			console.log(filter);
			if(filter.length > 0) {
				window.location.replace('http://localhost:49081/admin/user/sort?department_id='+filter+'&status=all');
				var filter = '';
				alert('Thêm mới thành công');				
			}	
		</script>
		@endsection
		