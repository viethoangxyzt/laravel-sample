@extends("backend.master.master")
@section("title")
{{ __('user.infor') }}
@endsection
@section("main")
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('user.infor') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">{{ __('user.infor') }}</div>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:40px">
                        <!-- Thông báo thêm thành công -->
                        @if(session()->has('success'))
                        <div class="alert bg-success" role="alert">
                            {{ session()->get('success') }}
                        </div>
                        @endif

                        <div class="col-md-6">

                            <div class="form-group">
                                <label>{{ __('user.image') }} </label>
                                <img id="avatar" class="thumbnail" width="20%" height="150px" src="@if($user->image==""){{ asset('uploads/no_avatar.png') }} @else {{ asset('uploads/'.$user->image) }} @endif">
                                <hr>
                            </div>
                            <div class="form-group">
                                <label>{{ __('user.name') }}</label>
                                <div>{{ $user['name'] }}</div>
                                <hr>
                            </div>

                            <div class="form-group">
                                <label>{{ __('user.department') }}</label>
                                <div>{{ $user->department->name }}</div>
                                <hr>
                            </div>
                            <div class="form-group">
                                <label>{{ __('user.email') }}</label>
                                <div>{{ $user['email'] }}</div>
                                <hr>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{ __('user.phone') }}</label>
                                <div>{{ $user['phone'] }}</div>
                                <hr>
                            </div>

                            <div class="form-group">
                                <label>{{ __('user.birthday') }}</label>
                                <div>{{ $user['birthday'] }}</div>
                                <hr>
                            </div>

                            <div class="form-group">
                                <label>{{ __('user.start_work') }}</label>
                                <div>{{ $user['start_work'] }}</div>
                                <hr>
                            </div>
                            @if($user['is_admin'] == config('common.IS_ADMIN'))
                            <div class="form-group">
                                <label>{{ __('user.role') }}</label>
                                <div>Admin</div>
                                <hr>
                            </div>
                            @endif
                            @if($user['is_manager'] == config('common.IS_MANAGER'))
                            <div class="form-group">
                                <label>Quản lí bộ phận</label>
                                <div>{{ $user->department->name}}</div>
                                <hr>
                            </div>
                            @endif
                            <div class="form-group">
                                <label>{{ __('user.status') }}</label>
                                @if($user['status']==config('common.IS_WORK')) <div>{{ __('user.is_work') }}</div> @endif
                                @if($user['status']==config('common.QUIT')) <div>{{ __('user.quit') }} </div> @endif
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if(Auth::user()->id == $user['id'])
                            <a href="{{ route('profile.update', $user['id']) }}"><button class="btn btn-infor">{{ __('user.edit')}}</button></a>
                            @endif
                            
                            @can('viewAdmin', \App\User::class)
                            @if(Auth::user()->id != $user->id)
                            <a href="{{ route('user.edit', $user['id']) }}"><button class="btn btn-infor">{{ __('user.edit')}}</button></a>
                            @endif
                            <a href=" {{ route('reset', $user['id']) }}"><button class="btn btn-infor" onclick="return confirm('Cập nhật mật khẩu cho {{ $user->name }}?')">{{ __('user.reset_password') }}</button></a>
                            @endcan
                            <a href="{{ route('profile.cv', $user['id']) }}"><button class="btn btn-infor">Xuất pdf</button></a>
                        </div>
                    </div>
                </div>
                @can('viewAdmin', \App\User::class)
                <div class="row" style="margin-left: 20px;">
                    <a style="margin-bottom: 20px;" class="btn btn-info" href="{{ route('user.index') }}" >{{ __('link.back') }}</a>
                </div>  
                @endcan
            </div>
        </div>
    </div>
    <!--/.row-->
</div>

<!--end main-->
@endsection
