@extends("backend.master.master")
@section("title")
{{ __('user.edit') }}
@endsection
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('user.edit') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">{{ __('user.edit') }}</div>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:40px">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('profile.store', $user['id']) }}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('user.image') }}</label>
                                    <input require id="img" type="file" name="image" class="form-control hidden" onchange="changeImg(this)">
                                    <img id="avatar" class="thumbnail" width="20%" height="100px" src="../uploads/{{ $user['image'] }}">
                                    @error('image')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('user.phone') }}</label>
                                    <input type="text" name="phone" class="form-control" value="{{ old('phone', $user['phone']) }}">
                                    @error('phone')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.birthday') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" name="birthday" class="form-control" value="{{ old('birthday', $user['birthday']) }}">
                                    @error('birthday')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-success" type="submit">{{ __('form.edit') }}</button>
                            <button class="btn btn-danger" type="reset">{{ __('link.cancel') }}</button>
                        </div>
                    </div>
                    @csrf
                    </form>
                </div>
                <div class="row" style="margin-left: 20px;">
                    <a style="margin-bottom: 20px;" class="btn btn-info" href="{{ url()->previous() }}">{{ __('link.back') }}</a>
                </div> 
            </div>
          
        </div>
    </div>
    <!--/.row-->
</div>

<!--end main-->
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
</script>
@endsection
