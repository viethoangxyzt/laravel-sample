@extends("backend.master.master")
@section("title")
{{ __('user.add') }}
@endsection
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('user.add') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">{{ __('user.add') }}</div>
                @if (session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
                @endif
                <div class="panel-body">
                    <div class="row" style="margin-bottom:40px">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('user.store') }}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('user.department') }} (<span class="text-danger">*</span>)</label>
                                    <select name="departments_id" class="form-control">
                                        @foreach($departments as $department)
                                        <option value="{{ $department['id'] }}">{{ $department['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.name') }} (<span class="text-danger">*</span>)</label>
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                    @error('name')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.email') }}</label>
                                    <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                                    @error('email')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.phone') }}</label>
                                    <input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
                                    @error('phone')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.birthday') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" name="birthday" class="form-control" value="{{ old('birthday') }}" >
                                    @error('birthday')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('user.image') }}</label>
                                    <input require id="img" type="file" name="image" class="form-control hidden" onchange="changeImg(this)">
                                    <img id="avatar" class="thumbnail" width="20%" height="100px" src="img/import-img.png">
                                    @error('image')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.start_work') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" name="start_work" class="form-control" value="{{ old('start_work') }}">
                                    @error('start_work')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.role') }}</label>
                                    <select name="is_admin" class="form-control">
                                        <option value="1">Admin</option>
                                        <option value="0">User</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.manager') }}</label>
                                    <select name="is_manager" class="form-control">
                                        <option value="0">Không</option>
                                        <option value="1">Có</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.status') }} (<span class="text-danger">*</span>)</label>
                                    <select name="status" class="form-control">
                                        <option value="1">{{ __('user.is_work') }}</option>
                                        <option value="0">{{ __('user.quit') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="margin-left: 20px;">
                                    <button class="btn btn-success" type="submit">{{ __('form.add') }}</button>
                                    <button class="btn btn-danger" type="reset">{{ __('link.cancel') }}</button>
                                </div>
                            </div>
                           
                            @csrf
                        </form>
                        
                    </div>
                    <div class="row" style="margin-left: 20px;">
                        <a class="btn btn-info" href="{{ route('user.index') }}">{{ __('link.back') }}</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        
    </div>
</div>
<!--/.row-->


<!--end main-->

<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });
    
</script>


@endsection
