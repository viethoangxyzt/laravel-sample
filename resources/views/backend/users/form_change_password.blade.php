<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ __('form.reset_password') }}</title>
	<base href="/backend/" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
</head>

<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">{{ __('form.reset_password') }}</div>
				<div class="panel-body">
					@if (session()->has('error'))
					<div class="alert alert-danger">
						{{ session()->get('error') }}
					</div>
					@endif
					<form role="form" method="POST" action="{{ route('changePassword') }}">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Mật khẩu hiện tại" name="old_password" type="password" autofocus="" value="{{ old('old_password') }}">
								@error('old_password')
								<p class="text-danger">{{ $message }}</p>
								@enderror
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Mật khẩu mới" name="password" type="password" value="{{ old('password') }}">
								@error('password')
								<p class="text-danger">{{ $message }}</p>
								@enderror
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Nhập lại mật khẩu" name="confirm_password" type="password" value="">
								@error('confirm_password')
								<p class="text-danger">{{ $message }}</p>
								@enderror
							</div>
							<button type="submit" class="btn btn-primary">{{ __('form.edit') }}</button>
							<button type="reset" class="btn btn-danger">{{ __('link.cancel') }}</button>
							@csrf
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->
</body>
</html>
