@extends("backend/master/master")
@section('title')
{{ __('project.title') }}
@endsection()
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><svg class="glyph stroked home">
						<use xlink:href="#stroked-home"></use>
					</svg></li>
			<li class="active">{{ __('project.list') }}</li>
		</ol>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{ __('project.list') }}</h1>
		</div>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-xs-12 col-md-12 col-lg-12">

			<div class="panel panel-primary">

				<div class="panel-body">
					<div class="bootstrap-table">
						<div class="table-responsive">
							@if (session()->has('success'))
							<div class="alert alert-success">
								{{ session()->get('success') }}
							</div>
							@endif

							@if (session()->has('error'))
							<div class="alert alert-danger">
								{{ session()->get('error') }}
							</div>
							@endif
							
                            {{-- @foreach($projectDepartments as $project) --}}
                            {{-- <div>{{ $user->name }}</div> --}}
							<table class="table table-bordered" style="margin-top:20px;">
								<thead>
									<tr class="bg-primary">	
										<th>{{ __('project.name') }}</th>
										<th>{{ __('project.description') }}</th>
                                        <th>{{ __('project.time') }}</th>   
                                        <th>{{ __('project.status') }}</th>
										<th>Nhân viên</th> 	
									</tr>
								</thead>
                               
								<tbody>
                                    @foreach($projectDepartments as $project)
									<tr>
									<td>{{ $project->name }}</td>
									<td>{{ $project->description }}</td>
									<td>{{date('d-m-Y', strtotime($project->start_date)) }} - {{date('d-m-Y', strtotime($project->end_date)) }}</td>
                                    <td>
                                        @if($project->status == config('common.IS_PENDING'))
                                        <p class="text-warning">{{ __('project.is_pending') }}</p>
                                        @elseif($project->status == config('common.IS_DEVELOPING'))
                                        <p class= "text-success">{{ __('project.is_developing') }}</p>
                                        @else
                                        <p class="text-info">{{ __('project.is_done') }}</p>
                                        @endif
                                    </td>
									<td>
										@foreach($project->users as $user)
										@if($user->departments_id == Auth::user()->departments_id)
										{{ $user->name }}
										<br>
										@endif
										@endforeach
								
									</td>
                                    
								</tr>
                                    @endforeach
								</tbody>
							</table>
                            {{-- @endforeach --}}
							<div align='right'>			
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

				</div>
			</div>
			<!--/.row-->
		</div>
	</div>
</div>
<!--end main-->

<!-- javascript -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
@endsection