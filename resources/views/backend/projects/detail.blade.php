@extends("backend/master/master")
@section("title")
{{ __('project.detail') }}
@endsection
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('project.detail') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i>{{ __('project.detail') }}</div>
                <div class="panel-body">
                    <div class="row justify-content-center" style="margin-bottom:40px">       
                            <div class="col-md-8 col-lg-8 col-lg-offset-1">
                                <div class="col-md-4 col-lg-4">
                                    <div>
                                        <p><b>{{ __('project.name') }}:</b> {{ $project->name }}</p>
                                    </div>
                                    <div>
                                        <p><b>{{ __('project.description') }}</b>
                                        {{ $project->description }}</p> 
                                    </div>
                                    <div>     
                                        <p><b>{{ __('project.status') }}:</b>
                                        @if($project->status == config('common.IS_PENDING')) <i class="text-warning">{{ __('project.is_pending') }}</i> @endif
                                        @if($project->status == config('common.IS_DEVELOPING')) <i class="text-success">{{  __('project.is_developing')  }}</i> @endif
                                        @if($project->status == config('common.IS_DONE')) <i class="text-info">{{ __('project.is_done') }}</i> @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-lg-offset-1">
                                    <div>
                                        <p><b>{{ __('project.start_date') }}:</b> {{ $project->start_date }}</p>
                                    </div>
                                    <div >
                                        <p><b>{{ __('project.end_date') }}:</b>{{ $project->end_date }}</p> 
                                    </div>
                                </div>
                            </div>  
                                      
                    </div>
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <div class="col-md-8 col-lg-8 col-lg-offset-1">
                            <h4>Danh sách thành viên</h4>
                            @if (session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                            @endif  
                            <a href="{{ route('project.user.formAdd', $project->id) }}" class="btn btn-primary">{{ __('user.add') }}</a>
                            <div class="table-responsive">
							<table class="table table-bordered" style="margin-top:20px;">
								<thead>
									<tr class="bg-primary">	
										<th>{{ __('user.name') }}</th>
										<th>{{ __('user.department') }}</th>
										<th>{{ __('project.start_date') }}</th>
										<th>{{ __('project.end_date') }}</th>                    
										<th width='25%'>Tùy chọn</th>
									</tr>
								</thead>
								<tbody>
                                @foreach($project->users as $user)
									<tr>
                                    <td><a href="{{ route('profile', $user->id) }}">{{ $user->name }}</a></td>
									<td>{{ $user->department->name }}</td>
									<td>{{date('d-m-Y', strtotime($user->pivot->start_user_date)) }}</td>
									<td>{{ date('d-m-Y', strtotime($user->pivot->end_user_date)) }}</td>
									<td>
										<a href="/admin/project/users/{{ $project->id }}/edit/{{ $user->id }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
										{{-- <a href="" class="btn btn-success"><i class="" aria-hidden="true"></i> Chi tiết </a> --}}
                                        <a href="/admin/project/users/{{ $project->id }}/delete/{{ $user->id }}" onclick="return confirm('Bạn muốn xoá thành viên {{ $user->name }}')"  class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
                                    </td>
								</tr> 
                                @endforeach
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left: 20px;">
                    <a style="margin-bottom: 20px;" class="btn btn-info" href="{{ route('project.index') }}" }}>{{ __('link.back') }}</a>
                </div>  
            </div>
        </div>
    </div>
    <!--/.row-->
</div>

<!--end main-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
@endsection
