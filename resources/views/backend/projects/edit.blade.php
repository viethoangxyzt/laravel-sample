@extends("backend/master/master")
@section("title")
{{ __('project.edit') }}
@endsection
@section("main")
<!--main-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('project.edit') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i>{{ __('project.edit') }}</div>
                <div class="panel-body">
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('project.update', $project->id) }}">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <!-- @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif -->
                              

                                <div class="form-group">
                                    <label>{{ __('project.name') }} (<span class="text-danger">*</span>)</label>
                                    <input type="text" name="name" class="form-control" value="{{ old('name',$project->name) }}">
                                    @error('name')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror


                                </div>
                                <div class="form-group">
                                    <label>{{ __('project.description') }}</label>
                                    <input type="text" name="description" class="form-control" value="{{ old('name',$project->description) }}">
                                </div>

                                <div class="form-group">
                                    
                                    <label>{{ __('project.status') }}</label>
                                    <select name="status" class="form-control">
                                        <option @if($project->status == config('common.IS_PENDING')) selected @endif value="1">{{ __('project.is_pending') }}</option>
                                        <option @if($project->status == config('common.IS_DEVELOPING')) selected @endif value="2">{{ __('project.is_developing') }}</option>
                                        <option @if($project->status == config('common.IS_DONE')) selected @endif value="3">{{ __('project.is_done') }}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{ __('project.start_date') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" name="start_date" class="form-control" value="{{ old('start_date', $project->start_date) }}">
                                    @error('start_date')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>{{ __('project.end_date') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" name="end_date" class="form-control" value="{{ old('end_date', $project->end_date) }}">
                                    @error('end_date')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right" style="margin-top: 20px;">
                                    <button class="btn btn-success" type="submit">{{ __('form.edit') }}</button>
                                    <button class="btn btn-danger" type="reset">{{ __('link.cancel') }}</button>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                    <div class="row" style="margin-left: 20px;">
                        <a style="margin-bottom: 20px;" class="btn btn-info" href="{{ url()->previous() }}">{{ __('link.back') }}</a>
                    </div> 

                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>

    <!--/.row-->
</div>

<!--end main-->

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
@endsection
