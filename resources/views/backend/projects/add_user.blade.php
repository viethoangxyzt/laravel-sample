@extends("backend/master/master")
@section("title")
{{ __('project.add') }}
@endsection
@section("main")
<!--main-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('user.add') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i>Thêm nhân viên vào dự án {{ $project->name }} Từ {{date('d-m-Y', strtotime($project->start_date)) }} đến {{date('d-m-Y', strtotime($project->end_date)) }} </div>
                <div class="panel-body">
                    
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('project.user.add', $project->id) }}">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                        {{-- messager error --}}
                                @if (session()->has('error'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('error') }}
                                    </div>
                                @endif
                                <div>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                                    <label>{{ __('user.name') }}(<span class="text-danger">*</span>)</label>
                                    <select name="user_id" id="siteID" class="form-control" style="width:100%">
                                        @foreach($users as $user)
                                        <option value='{{ $user['id'] }}' selected='true'> {{ $user['name'] }} </option>
                                        @endforeach
                                    </select>
                                    @error('user_id')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('project.start_date') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" name="start_user_date" class="form-control" value="{{ old('start_user_date') }}">
                                    @error('start_user_date')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('project.end_date') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" name="end_user_date" class="form-control" value="{{ old('end_user_date') }}">
                                    @error('end_user_date')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div>
                                    <input type="hidden" name ="start_project_date" value="{{ $project->start_date }}">
                                </div>
                                <div>
                                    <input type="hidden" name ="end_project_date" value="{{ $project->end_date }}">
                                </div>
                                <div>
                                    <input type="hidden" name ="project_id" value="{{ $project->id }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right" style="margin-top: 20px;">
                                    <button href="" class="btn btn-success" type="submit">{{ __('form.add') }}</button>
                                    <button href="" class="btn btn-danger" type="reset">{{ __('link.cancel') }}</button>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!--/.row-->
</div>
<!--end main-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
