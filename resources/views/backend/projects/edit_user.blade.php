@extends("backend/master/master")
@section("title")
{{ __('project.edit') }}
@endsection
@section("main")
<!--main-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('user.edit') }} dự án {{ $project->name }} </h1>
        </div>  
        <div class="col-lg-3">
            Từ {{date('d-m-Y', strtotime($project->start_date)) }} đến {{date('d-m-Y', strtotime($project->end_date)) }}
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i>Thay đổi thông tin nhân viên</div>
                <div class="panel-body">    
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <form method="POST" enctype="multipart/form-data" action="/admin/project/users/{{ $project->id }}/update/{{ $user->id }}">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                        {{-- messager error --}}
                                @if (session()->has('error'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('error') }}
                                    </div>
                                @endif
                                 <div>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                                    <label>{{ __('user.name') }}: </label>
                                    <b>{{ $user->name }}</b>
                                </div>  
                                @foreach($userProject as $userProject)                           
                                <div class="form-group">
                                    <label>{{ __('project.start_date') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" name="start_user_date" class="form-control" value="{{ old('start_user_date', $userProject->start_user_date) }}">
                                    @error('start_user_date') 
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('project.end_date') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" name="end_user_date" class="form-control" value="{{ old('end_user_date', $userProject->end_user_date) }}">
                                    @error('end_user_date')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right" style="margin-top: 20px;">
                                    <button href="" class="btn btn-success" type="submit">{{ __('form.edit') }}</button>
                                    <button href="" class="btn btn-danger" type="reset">{{ __('link.cancel') }}</button>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!--/.row-->
</div>
<!--end main-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
