@extends("backend/master/master")
@section('title')
{{ __('department.title') }}
@endsection()
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><svg class="glyph stroked home">
						<use xlink:href="#stroked-home"></use>
					</svg></li>
			<li class="active">{{ __('department.list') }}</li>
		</ol>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{ __('department.list') }}</h1>
		</div>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-xs-12 col-md-12 col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="bootstrap-table">
						<div class="table-responsive">
							@if (session()->has('success'))
							<div class="alert alert-success">
								{{ session()->get('success') }}
							</div>
							@endif

							@if (session()->has('error'))
							<div class="alert alert-danger">
								{{ session()->get('error') }}
							</div>
							@endif

							<a href="{{ route('department.create') }}" class="btn btn-primary">{{ __('department.add') }}</a>
							<a href="{{ route('department.formAddUser') }}" class="btn btn-primary">{{ __('department.addUser') }}</a>
							<table class="table table-bordered" style="margin-top:20px;">
								<thead>
									<tr class="bg-primary">	
										<th>{{ __('department.name') }}</th>
										<th>{{ __('department.description') }}</th>
										<th>{{ __('department.manager') }}</th>
										<th width='18%'>Tùy chọn</th>
									</tr>
								</thead>
								<tbody>
									@foreach($departments as $department)
									<tr>
										<td>{{ $department->name }}</td>
										<td>{{ $department->description }}</td>
										<td>
											@foreach($department->users as $user)
											@if($user->is_manager == config('common.IS_MANAGER') )
											<a href=" {{ route('profile', $user->id) }}">{{ $user->name }}</a>
											<br>
											@else
											@endif
											@endforeach
										</td>
										<td>
											<a href="{{ route('department.edit', $department->id) }}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
											<a href="{{ route('department.delete', $department->id) }}" onclick="return confirm('Bạn muốn xoá bộ phận {{ $department->name }}')"  class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							<div align='right'>
								{{ $departments->links("pagination::bootstrap-4 ") }}
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!--/.row-->
		</div>
	</div>
</div>
<!--end main-->

@endsection
