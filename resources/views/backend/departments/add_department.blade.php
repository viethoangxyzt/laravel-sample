@extends("backend/master/master")
@section("title")
{{ __('department.add') }}
@endsection
@section("main")
<!--main-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('department.add') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i>{{ __('department.add') }}</div>
                <div class="panel-body">
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('department.store') }}">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <!-- @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif -->
                              
                                <div class="form-group">
                                    <label>{{ __('department.name') }} (<span class="text-danger">*</span>)</label>
                                    <input type="text" name="name" class="form-control">
                                    @error('name')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror


                                </div>
                                <div class="form-group">
                                    <label>{{ __('department.description') }}</label>
                                    <input type="text" name="description" class="form-control">
                                </div>

                                <div>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                                    <label>{{ __('department.manager') }}</label>
                                    <select name="manager" id="siteID" class="form-control" style="width:100%">
                                        @foreach($users as $user)
                                        <option value='{{ $user['id'] }}' selected='true'> {{ $user['name'] }} </option>
                                        @endforeach
                                        <option value='' selected='true'> </option>
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right" style="margin-top: 20px;">
                                    <button class="btn btn-success" type="submit">{{ __('form.add') }}</button>
                                    <button class="btn btn-danger" type="reset">{{ __('link.cancel') }}</button>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                    <div class="row" style="margin-left: 20px; margin-top: -40px">
                        <a class="btn btn-info" href="{{ route('department.index') }}">{{ __('link.back') }}</a>
                    </div>
                </div>

                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>

    <!--/.row-->
</div>

<!--end main-->
<script>
    $(function() {
        $("select").select2();
    });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

@endsection
