@extends("backend/master/master")
@section("title")
{{ __('department.add') }}
@endsection
@section("main")
<!--main-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('department.addUser') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fas fa-user"></i>{{ __('department.addUser') }}</div>
                <div class="panel-body">
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('department.addUser') }}">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>{{ __('department.name') }}</label>
                                    <select name="department" class="form-control">
                                        @foreach($departments as $department)
                                        <option value="{{ $department['id'] }}">{{ $department['name'] }}</option>
                                        @endforeach
                                    </select>
                                    @error('department')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                                    <label>{{ __('user.name') }}</label>
                                    <select name="users[]" id="siteID" class="form-control js-example-basic-multiple" style="width:100%" multiple="multiple">
                                    </select>
                                    @error('users')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right" style="margin-top: 20px;">
                                    <button class="btn btn-success" type="submit">{{ __('form.add') }}</button>
                                </div>
                            </div>
                            @csrf
                        </form>
                    </div>
                    <div class="row" style="margin-left: 20px; margin-top: -40px">
                        <a class="btn btn-info" href="{{ route('department.index') }}" }}>{{ __('link.back') }}</a>
                    </div>
                </div>
                    <div class="clearfix"></div>
                    <input id="auth_id" value="{{ Auth::user()->id }}" hidden>
                </div>
            </div>

        </div>
    </div>

    <!--/.row-->
</div>

<!--end main-->
<script>
   $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
        var auth_id = $('#auth_id').val();

        $.ajax({
            url: 'http://localhost:49081/api/users/list',
            type: 'GET',
            success: function(rs){
                var str ='';
                $.each(rs.data, function(i, item){
                    if(item.id != auth_id)
                    {
                    str += `<option value="${item.id}">${item.name}</option>`;
                    }
                });
                $('#siteID').html(str);
            }
        });
    });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" 
integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A=="
 crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
