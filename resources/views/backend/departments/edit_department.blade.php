@extends("backend/master/master")
@section("title")
{{ __('department.edit') }}
@endsection
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('department.edit') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fa-solid fa-apartment"></i>  {{ __('link.edit_department') }} - {{ $department['name'] }}</div>
                <div class="panel-body">
                    <div class="row justify-content-center" style="margin-bottom:40px">
                        <form method="POST" enctype="multipart/form-data" action=" {{ route('department.update', $department['id']) }}">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">

                                <div class="form-group">
                                    <label>{{ __('department.name') }}(<span class="text-danger">*</span>)</label>
                                    <input type="text" name="name" class="form-control" placeholder="{{ __('form.input_name') }}" value="{{ old('name', $department['name']) }}">
                                    @error('name')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>{{ __('department.description') }}</label>
                                    <input type="text" name="description" class="form-control" placeholder="{{ __('form.input_description') }}" value="{{ $department['description'] }}">
                                    @error('description')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div>
                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                                    <label>{{ __('department.manager') }}</label>
                                    <select name="manager" id="siteID" class="form-control" style="width:100%">                                      
                                        @foreach($users as $user)
                                        <option  {{ $user['is_manager'] == config('common.IS_MANAGER') ? 'selected' : ''}}  value='{{ $user["id"] }}'> {{ $user['name'] }} </option>
                                        @endforeach
                                    </select>
                                    @error('manager')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right" style="margin-top: 20px;">
                                    <button class="btn btn-success" type="submit">{{ __('link.edit') }}</button>
                                    <button class="btn btn-danger" type="reset">{{ __('link.cancel') }}</button>    
                                </div>
                            </div>

                            @csrf
                        </form>
                    </div>
                    <div class="row" style="margin-left: 20px; margin-top: -40px">
                        <a class="btn btn-info" href="{{ route('department.index') }}" }}>{{ __('link.back') }}</a>
                    </div>
                </div>

                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>

    <!--/.row-->
</div>

<!--end main-->
<script>
    $(function() {
        $("select").select2();
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>



@endsection
