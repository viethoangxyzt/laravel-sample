@extends("backend/master/master")
@section("title")
{{ __('user.title') }}
@endsection
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><svg class="glyph stroked home">
						<use xlink:href="#stroked-home"></use>
					</svg></li>
			<li class="active">{{ __('user.list') }}</li>
		</ol>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{ __('user.list') }}</h1>
		</div>
	</div>
	<!--/.row-->
	<div class="row">
		<div class="col-xs-12 col-md-12 col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="bootstrap-table">
						<div class="table-responsive">
							<!-- Thông báo thành công -->
							@if (session()->has('success'))
							<div class="alert alert-success">
								{{ session()->get('success') }}
							</div>
							@endif
							<!-- Thông báo lỗi -->
							@if (session()->has('error'))
							<div class="alert alert-danger">
								{{ session()->get('error') }}
							</div>
							@endif
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							<div id=alert></div>
							
							@if (session('list_department_excel') && session('list_department_excel')!=0)
            					<div class="alert alert-success alert-dismissible">
                					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                					Danh Sách Phòng tạo mới :
                					<ul class="">
                    					@foreach (session('list_department_excel') as $departerment)
                    						<li class="">{{ $departerment }}</li>
                    					@endforeach
                					</ul>
            					</div>
            				@endif

							@can('viewAdmin', \App\User::class)
							<a href=" {{ route('user.api.add') }}" class="btn btn-primary">{{ __('user.add') }}</a>
							
							<a href="{{ route('user.createExcel') }}" class="btn btn-primary">Thêm
									Excel</a>
							
							<div style="margin-left: 300px; margin-top: -30px;">
								<!-- sort form -->
								<form type="get" action="">
									<select id ="sort" name="sort" style="height: 30px; margin: 0 20px;">
										<option selected disabled>Sắp xếp</option>
										<option value="sort_by_id" {{ (isset($_GET['sort']) && $_GET['sort'] == 'sort_by_id') ? 'selected' : '' }}>Sắp xếp theo ngày tạo</option>
										<option value="birthday_asc" {{ (isset($_GET['sort']) && $_GET['sort'] == 'birthday_asc') ? 'selected' : '' }}>Ngày sinh tăng dần</option>
										<option value="birthday_desc" {{ (isset($_GET['sort']) && $_GET['sort'] == 'birthday_desc') ? 'selected' : '' }}>Ngày sinh giảm dần</option>
										<option value="start_work_asc" {{ (isset($_GET['sort']) && $_GET['sort'] == 'start_work_asc') ? 'selected' : '' }}>Ngày làm việc tăng dần</option>
										<option value="start_work_desc" {{ (isset($_GET['sort']) && $_GET['sort'] == 'start_work_desc') ? 'selected' : '' }}>Ngày làm việc giảm dần</option>
									</select>
									<select id = "department_id" name="department_id" style="height: 30px;  margin: 0 20px;">
										<option value="0" >{{ __('user.department') }}</option>
										@foreach($departments as $department)
										<option value="{{ $department['id'] }}" {{ isset($_GET['department_id']) && $_GET['department_id'] == $department['id'] ? 'selected' : '' }}>{{ $department['name'] }}</option>
										@endforeach
									</select>
									<select id="status" name="status" style="height: 30px;  margin: 0 20px;">
										<option value="all" {{ isset($_GET['status']) && $_GET['status'] == 'all' ? 'selected' : '' }}>{{ __('user.status') }}</option>
										<option value="work" {{ isset($_GET['status']) && $_GET['status'] == 'work' ? 'selected' : '' }}>{{ __('user.is_work') }}</option>
										<option value="out" {{ isset($_GET['status']) && $_GET['status'] == 'out' ? 'selected' : '' }}>{{ __('user.quit') }}</option>
									</select>
									<button type="submit" class="btn btn-success" style="height: 30px; line-height:1;">Lọc</button>
								</form>
								<!-- Sort form -->

							</div>
							<div style="margin-left: 300px; margin-top: 20px;">
								<form action="" method="get">
									<input type="text" id="search"  placeholder="Tìm kiếm theo tên" name="search" value="{{ isset($_GET['search']) ? $_GET['search'] : '' }}" title="Type in a name" style="height: 30px; margin: 0 20px;">
									<button type="submit" class="btn btn-success" style="height: 30px; line-height:1;">Tìm kiếm</button>

								</form>
							</div>
							@endcan
							<table id="table-user" class="table table-bordered" style="margin-top:20px;">
								<thead>
									<tr class="bg-primary">
										<th class="text-center">{{ __('user.image') }}</th>
										<th class="text-center">{{ __('user.name') }}</th>
										<th class="text-center">{{ __('user.birthday') }}</th>
										<th class="text-center">{{ __('user.email') }}</th>
										<th class="text-center">{{ __('user.phone') }}</th>
										<th class="text-center">{{ __('user.department') }}</th>
										<th class="text-center">{{ __('user.start_work') }}</th>
										<th class="text-center">{{ __('user.status') }}</th>
										<th class="text-center">{{ __('user.role') }}</th>
										@can('viewAdmin', \App\User::class)
										<th class="text-center" width='12%'>Tùy chọn</th>
										@endcan
									</tr>
								</thead>
								<tbody id="load_data">
									
								</tbody>
							</table>
							<div align='right' id="paginate">
							</div>
						</div>
						@can('viewAdmin', \App\User::class)
						<form>
							<a class="btn btn-success" id="export_excel" >{{ __('link.export') }}</a>
						</form>
						@endcan
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!--/.row-->
		</div>
		<!--end main-->

		<!-- Button trigger modal -->
  
  <!-- Modal -->
  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Xác nhận xóa</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
		  <button id="confirm" type="button" data-id="" class="btn btn-primary delete-confirm">Xóa</button>
		</div>
	  </div>
	</div>
  </div>


	<script>
		//get url query
		var url = window.location.search;
		// regex for "?page=."
		var regex = new RegExp("[?&]page=([^&#]*)");
		//if url has "?page=." then remove it
		(regex.test(url)) ? url = url.replace(regex, ''): '';
		//set url for export
		var url_export_excel = "{{route('user.export')}}" + url;
		//change href for export excel
		$('#export_excel').attr('href', url_export_excel);
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<script>
		var api_url;
		// get current page
		var page = window.location.href.match(/\d+$/);

		// set pre_page
		let pre_page = parseInt(page) -1;
		if(pre_page <=0) {
			pre_page = 1;
		}
		// set next_page
		let next_page = parseInt(page) + 1;
    
		// get keyword in input search
		var keyword = $('#search').val();

        current_url = window.location.href;
		if(page != null){
			url_paginate = current_url.substring(0, current_url.length-1);
		} else {
			url_paginate = current_url + '?page=';
		}
        
		// get value sort 
		var department_id = $('#department_id').val();
		var status = $('#status').val();
		var sort = $('#sort').val();
		if(status == 'work'){
			status = 1;
		} 
		if(status == 'out'){
			status = 0;
		} 
		console.log('status =' + status);
	
		var sort = $('#sort').val();

		$(document).ready(function(){	

			// load list data in views
			if(keyword != null && keyword !="")
			{ 
				var index = keyword.indexOf('?');
				keyword = keyword.slice(0, index-1);
                api_url = 'http://localhost:49081/api/users/search/'+keyword+'?page='+page;
				LoadData(page);
				
			} else if (department_id != null)
			{ 
				api_url = 'http://localhost:49081/api/users/department/'+department_id+'/status/'+status+'/sort/'+sort+'?page='+page;
				LoadData(page);
			} else
			{
				api_url = 'http://localhost:49081/api/users?page=1'+page;
				LoadData(page);

			}	
		});

		var user_id;

		$(document).on('click', '.delete', function(){
			user_id = $(this).data('id');

			$('#confirm').click(function(){
				$.ajax({
					url : 'http://localhost:49081/api/users/'+user_id,
					type: 'DELETE',
					success:function(){
						// toastr.success('Delete successfully !');
						$('#alert').append('success');
					}
				});

				$('#deleteModal').hide('modal');
				window.location.reload();
				alert('Đã xóa thành công');				
			});
					
		});

		
        // load data from api
		function LoadData(page)
    	{	
			
			$.ajax({
				url: api_url,
				type:'GET',
				success:function(rs){				
					var str="";
					console.log(rs);
					$.each(rs.data, function(i, item){
						str += "<tr>"; 
                    	if(item.image == null){
                        	str += '<td><img src ="{{ asset("uploads/no_avatar.png") }}" alt="Avatar" width="50px" class="thumbnail"></td>'
                    	}  else {
                        	var test = 
                        	str += `<td><img src ="{{ asset("uploads/`+item.image+`") }}" alt="Avatar" width="50px" class="thumbnail"></td>`
                    	}               
						str += `<td><a href ="${item.url_profile}">${item.name}</a></td>`;
						str += "<td>" + item.birthday + "</td>";
						str += "<td>" + item.email + "</td>";
						str += "<td>" + item.phone + "</td>";
						str += "<td>" + item.department_name + "</td>";
						str += "<td>" + item.start_work + "</td>";
						if(item.status == 1){
                        	str += '<td class ="text-info">Đang làm việc</td>';
                    	} else {
                        	str += '<td class ="text-danger">Đã nghỉ việc</td>';
                    	}
						if(item.is_admin == 1){
                        	str += '<td>Admin</td>';
                    	} else if(item.is_manager == 1) {
                        	str += '<td>Quản lý</td>';
                    	} else {
                        	str += '<td>Nhân viên</td>';
                    	}
						str += '<td class ="text-center">';
                    	str += `<a href="${item.url_edit}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a>`;			
						str += `<button data-id="${item.id}" data-url="${item.url_delete}" data-name = "${item.name}" type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash" aria-hidden="true"></i></button>`;
                    	str += `<a href="${item.url_resetpass}" onclick = "return confirm('Reset mật khẩu cho ${item.name}?')" class="btn btn-primary"><i class="fas fa-key" aria-hidden="true"></i></a>`;
                    	str += '</td>';
						str += '</tr>';
					});
					$('#load_data').html(str);
					var paginate = "";
					var total_pages = Math.ceil(rs.total/3);
					if(total_pages>1)
					{
						if(next_page > total_pages) {

							next_page = total_pages;
						}
						paginate += '<nav aria-label="Page navigation example">';
						paginate += '<ul class="pagination justify-content-center">';
						paginate += `<li class="page-item"><a class="page-link" href="`+url_paginate+pre_page+`"><</a></li>`;
						for(let i =1; i<= total_pages; i++)
						{   	
							if(page == i || page ==null && i==1){
							paginate += `<li class="page-item active"><a class="page-link" href="`+ url_paginate+i+`">`+i+`</a></li>`;
							} else {
								paginate += `<li class="page-item"><a class="page-link" href="`+ url_paginate+i+`">`+i+`</a></li>`;
							}
						}
						paginate += `<li class="page-item"><a class="page-link" href="`+url_paginate+next_page+`">></a></li>`;
						paginate += '</ul>';
						paginate += '</nav>';
						$('#paginate').html(paginate);
					}
					
				}
			});
		}
		</script>
@endsection
