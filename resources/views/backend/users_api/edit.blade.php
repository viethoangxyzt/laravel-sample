@extends("backend.master.master")
@section("title")
{{ __('user.edit') }}
@endsection
@section("main")
<!--main-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('user.edit') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">{{ __('user.edit') }}</div>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:40px">
                        <form enctype="multipart/form-data" id ="editForm">
                         
                        </form>
                </div>
                <div class="row" style="margin-left: 20px;">
                    <a style="margin-bottom: 20px;" class="btn btn-info" href="{{ url()->previous() }}">{{ __('link.back') }}</a>
                </div>              
            </div>
        </div>
    </div>

    <!--/.row-->
</div>

<!--end main-->
        
<script src="js/bootstrap.min.js"></script>

<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
                var reader = new FileReader();
                //Sự kiện file đã được load vào website
                    eader.onload = function(e) {
                    //Thay đổi đường dẫn ảnh
                    $('#avatar').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function() {
            $('#avatar').click(function() {
                $('#img').click();
        });
    });
</script>

    
<script>
    $(document).ready(function(){ 
        var user_id = parseInt(window.location.href.match(/\d+$/));
        console.log(user_id);

        $.ajax({
            url : 'http://localhost:49081/api/users/'+user_id,
            type : 'GET',
            success : function(response){
                var str = "";
                var user = response.data;
                console.log(user);
                str += `<div class="col-md-6">`;
                str += `<div class="form-group">`;
                str += `<label>Phòng ban</label>`;
                str += `<select id ="department_id" name="departments_id" class="form-control">`;
                str += `</select>`;
                str += `</div>`;
                str += `<div class="form-group">`;
                str += `<label>Tên(<span class="text-danger">*</span>)</label>`;
                str += `<input type="text" name="name" class="form-control" value="${user.name}">`;              
                str += `</div>`;
                str += `<div class="form-group">`;
                str += `<label>Email(<span class="text-danger">*</span>)</label>`;
                str += `<input type="text" name="email" class="form-control" value="${user.email}">`;
                str += `</div>`;
                str += `<div class="form-group">`;
                str += `<label>Điện thoại</label>`;
                str += `<input type="text" name="phone" class="form-control" value="${user.phone}">`;
                str += `</div>`;
                str += `<div class="form-group">`;
                str += `<label>Sinh Nhật(<span class="text-danger">*</span>)</label>`;
                str += `<input type="date" name="birthday" class="form-control" value="${user.birthday}">`;
                str += `</div>`;
                str += `</div>`;
                str += `<div class="col-md-6">`;
                str += `<div class="form-group">`;
                str += `<label>Ảnh</label>`;
                str += `<input require id="img" type="file" name="image" class="form-control hidden" onchange="changeImg(this)">`;
                if(user.image == null)
                {
                    str += `<img id="avatar" class="thumbnail" width="20%" height="120px" src="../uploads/no_avatar.png">`;
                } else
                {
                    str += `<img id="avatar" class="thumbnail" width="20%" height="120px" src="../uploads/${user.image}">`;
                }
                str += `</div>`;
                str += `<div class="form-group">`;
                str += `<label>Ngày bắt đầu (<span class="text-danger">*</span>)</label>`;
                str += `<input type="date" name="start_work" class="form-control" value="${user.start_work}">`;
                str += `</div>`;
                str += `<div class="form-group">`;
                str += `<label>Vai trò</label>`;
                str += `<select name="is_admin" class="form-control">`;
                if(user.is_admin == 1)
                {
                    str += `<option value="1" selected>Admin</option>`;
                    str += `<option value="0">User</option>`;
                } else {
                    str += `<option value="1">Admin</option>`;
                    str += `<option value="0" selected>User</option>`;
                }
                str += `</select>`;
                str += `</div>`;
                str += `<div class="form-group">`;
                str += `<label>Quản lí</label>`;
                str += `<select name="is_manager" class="form-control">`;
                if(user.is_manager == 0)
                {
                    str += `<option value="0" selected>Không</option>`;
                    str += `<option value="1">Có</option>`;
                } else {
                    str += `<option value="0">Không</option>`;
                    str += `<option value="1" selected>Có</option>`;


                    }      
                str += `</select>`;
                str += `</div>`;
                str += `<div class="form-group">`;
                str += `<label>Trạng thái(<span class="text-danger">*</span>)</label>`;
                str += `<select name="status" class="form-control">`;
                if(user.status == 1)
                {
                    str += `<option value="1" selected>Đang làm việc </option>`;
                    str += `<option value="0">Đã nghỉ việc</option>`;
                } else {
                    str += `<option value="1">Đang làm việc </option>`;
                    str += `<option value="0" selected>Đã nghỉ việc</option>`;
                }  
                str += `</select>`;
                str += `</div>`;
                str += `</div>`;
                str += `</div>`;
                str += `<div class="row">`;
                str += `<div class="col-md-12">`;
                str += `<button class="btn btn-success" type="submit">Cập nhật </button>`;
                str += `<button class="btn btn-danger" type="reset">Hủy</button>`;
                str += `</div>`;
                str += `</div>`;
                str += `@csrf`;

                $.ajax({
                    url: 'http://localhost:49081/api/departments',
                    type: 'GET',
                    success: function(rs){
                    var selectDepartment = "";
                     $.each(rs.data, function(i, item){                             
                    if(user.departments_id == item.id)
                    {
                        selectDepartment += `<option value ="${item.id}" selected>${item.name}</option>`;                      
                    } else {
                        selectDepartment += `<option value ="${item.id}">${item.name}</option>`;                      
                    }
                    });
                    $('#department_id').html(selectDepartment);
                    }
                });
                $('#editForm').html(str);
            }
            
        });

        $.validator.addMethod("number_only", function(value, element) {
            return this.optional(element) || /^[0-9-]+$/.test(value);
        });

        if($('#editForm').length >0){
            $("#editForm").validate({
                rules: {
                    "name": {
                        required: true
                    },
                    "email": {
                        required: true,
                        email: true
                    },
                    "birthday": {
                        required: true
                    },
                    "start_work": {
                        required: true
                    },
                    "password": {
                        minlength: 6,
                        maxlength: 30
                    },
                    "phone":{
                        minlength: 10,
                        maxlength: 10,
                        number_only: true
                    },
                    "status": {
                        required: true
                    }
                },

                messages : {
                    "name": {
                        required: "Tên không được để trống"
                    },
                    "email": {
                        required: "Email không được để trống",
                        email: "Email phải đúng định dạng"
                    },
                    "birthday": {
                        required: "Ngày sinh không được để trống",
                    },
                    "start_work": {
                        required: "Ngày bắt đầu làm việc không được để trống"
                    },
                    "password": {
                        minlength: "Mật khẩu tối thiểu 6 kí tự",
                        maxlength: "Mật khẩu tối đa 30 kí tự"
                    },
                    "phone": {
                        minlength: "Số điện thoại phải có 10 kí tự",
                        maxlength: "Số điện thoại phải có 10 kí tự",                        
                    },
                    "status": {
                        required: "Trạng thái làm việc không được để trống"
                    }
                },
            });
        }
    });

    $('form').on('submit', function(){
        var user = $('#editForm').serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
            }, {});
        console.log(user);
        console.log(typeof(user));
        if($('#editForm').valid())
        {
            $.ajax({
                url: 'http://localhost:49081/api/processImage',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success : function(response){
                    user['image'] = response;
                }
            });
        }

            console.log(user);
            $.ajax({
                url : 'http://localhost:49081/api/users/'+user_id,
                type : 'PUT',
                data: user,
                success : function(){
                    alert("Thêm thành công");
                    window.location.replace("http://localhost:49081/users");
                }
            });
        
    });    
</script>


@endsection
