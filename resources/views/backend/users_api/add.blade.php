@extends("backend.master.master")
@section("title")
{{ __('user.add') }}
@endsection
@section("main")
<!--main-->
<style>
    .error {
        color: red;
        font-style: italic;
        font-weight: 500;
    }
</style>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ __('user.add') }}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">{{ __('user.add') }}</div>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:40px">
                        <form id="addForm" name ="form" role = "form"> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('user.department') }} (<span class="text-danger">*</span>)</label>
                                    <select id="department_id" name="departments_id" class="form-control">       
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.name') }} (<span class="text-danger">*</span>)</label>
                                    <input type="text" id ="name" name="name" class="form-control" value="{{ old('name') }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.email') }}</label>
                                    <input type="text" id="email" name="email" class="form-control" value="{{ old('email') }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.phone') }}</label>
                                    <input type="text" id="phone" name="phone" class="form-control" value="{{ old('phone') }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.birthday') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" id="birthday" name="birthday" class="form-control" value="{{ old('birthday') }}" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ __('user.image') }}</label>
                                    <input require id="image-input" type="file" name="image" class="form-control hidden" onchange="changeImg(this)">
                                    <img id="avatar" class="thumbnail" width="20%" height="100px" src="img/import-img.png">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.start_work') }} (<span class="text-danger">*</span>)</label>
                                    <input type="date" id="start_work" name="start_work" class="form-control" value="{{ old('start_work') }}">
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.role') }}</label>
                                    <select name="is_admin" id="is_admin" class="form-control">
                                        <option value="1">Admin</option>
                                        <option value="0">User</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.manager') }}</label>
                                    <select name="is_manager" id="is_manager" class="form-control">
                                        <option value="0">Không</option>
                                        <option value="1">Có</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('user.status') }} (<span class="text-danger">*</span>)</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="1">{{ __('user.is_work') }}</option>
                                        <option value="0">{{ __('user.quit') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="margin-left: 20px;">
                                    <button id="submit" class="btn btn-success" type="button">{{ __('form.add') }}</button>
                                    <button class="btn btn-danger" type="reset">{{ __('link.cancel') }}</button>
                                </div>
                            </div>                     
                            @csrf
                        </form> 
                    </div>
                    <div class="row" style="margin-left: 20px;">
                        <a class="btn btn-info" href="{{ url()->previous() }}">{{ __('link.back') }}</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        
    </div>
</div>
<!--/.row-->


<!--end main-->

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script>
    function changeImg(input) {
        //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            //Sự kiện file đã được load vào website
            reader.onload = function(e) {
                //Thay đổi đường dẫn ảnh
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#image-input').click();
        });
    }); 
</script>

<script>
    $(document).ready(function(){
        $.ajax({
            url: 'http://localhost:49081/api/departments',
            type: 'GET',
            success: function(rs){
                var selectDepartment = "";
                $.each(rs.data, function(i, item){
                    selectDepartment += `<option value ="${item.id}">${item.name}</option>`;
                });
                $('#department_id').html(selectDepartment);
            }
        });

        $.validator.addMethod("number_only", function(value, element) {
            return this.optional(element) || /^[0-9-]+$/.test(value);
        });

        if($('#addForm').length >0){

            $("#addForm").validate({
                rules: {
                    "name": {
                        required: true
                    },
                    "email": {
                        required: true,
                        email: true
                    },
                    "birthday": {
                        required: true
                    },
                    "start_work": {
                        required: true
                    },
                    "password": {
                        minlength: 6,
                        maxlength: 30
                    },
                    "phone":{
                        minlength: 10,
                        maxlength: 10,
                        number_only: true
                    },
                    "status": {
                        required: true
                    }
                },

                messages : {
                    "name": {
                        required: "Tên không được để trống"
                    },
                    "email": {
                        required: "Email không được để trống",
                        email: "Email phải đúng định dạng"
                    },
                    "birthday": {
                        required: "Ngày sinh không được để trống",
                    },
                    "start_work": {
                        required: "Ngày bắt đầu làm việc không được để trống"
                    },
                    "password": {
                        minlength: "Mật khẩu tối thiểu 6 kí tự",
                        maxlength: "Mật khẩu tối đa 30 kí tự"
                    },
                    "phone":{
                        minlength: "Số điện thoại phải có 10 kí tự",
                        maxlength: "Số điện thoại phải có 10 kí tự",                        
                    },
                    "status": {
                        required: "Trạng thái làm việc không được để trống"
                    }

                },
            });
        }
    });

    $('#submit').on('click', function(){

        if($('#addForm').valid())
        {
            const imageInput = $('#image-input')[0];
            const formData = new FormData();
            if(imageInput.files.length != 0) 
            {  
                formData.append('image', imageInput.files[0]);
            }
            formData.append('name', $('#name').val());
            formData.append('email', $('#email').val());
            formData.append('phone', $('#phone').val());
            formData.append('birthday', $('#birthday').val());
            formData.append('start_work', $('#start_work').val());
            formData.append('departments_id', $('#department_id').val());
            formData.append('is_admin', $('#is_admin').val());
            formData.append('is_manager', $('#is_manager').val());
            formData.append('status', $('#status').val());
            $.ajax({
                url: 'http://localhost:49081/api/users',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success : function(data){
                    console.log(data);
                    alert("Thêm thành công");
                    window.location.replace("http://localhost:49081/users");                       
                }
            });  
        }
    });
</script>


@endsection
