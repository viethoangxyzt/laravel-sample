<table>
    <thead>
        <tr>
            <th>name</th>
            <th>email</th>
            <th>phone</th>
            <th>birthday</th>
            <th>start_work</th>
            <th>department</th>
            <th>status</th>
            <th>position</th>
            <th>admin</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->phone }}</td>
            <td>{{ date('d-m-Y', strtotime($user->birthday)) }}</td>
            <td>{{ date('d-m-Y', strtotime($user->start_work)) }}</td>
            <td>{{ $user->department->name }}</td>
            @if($user->status == 1)
            <td>Đang làm việc</td>
            @else
            <td>Nghỉ việc</td>
            @endif
            @if($user->is_manager)
            <td>Quản lý</td>
            @else
            <td>Nhân viên</td>
            @endif
            @if($user->is_admin)
            <td>TRUE</td>
            @else
            <td>FALSE</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
