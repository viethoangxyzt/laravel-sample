@extends("backend/master/master")
@section("title")
@endsection
@section("main")
<style>
    .error {
        color: red;
        font-style: italic;
        font-weight: 500;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Thêm user Excel</h3>
                </div>
                @if (session()->has('failures'))

                <table class="table table-danger">
                    <tr>
                        <th>Hàng</th>
                        <th>Cột</th>
                        <th>Lỗi</th>
                        <th>Giá Trị Lỗi</th>
                    </tr>
                    @foreach (session()->get('failures') as $validation)
                    <tr>
                        <td>{{ $validation->row() }}</td>
                        <td>{{ $validation->attribute() }}</td>
                        <td class="text-danger">
                            <ul>
                                @foreach ($validation->errors() as $e)
                                <li>{{ $e }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td class="text-danger">
                            {{ $validation->values()[$validation->attribute()] }}
                        </td>
                    </tr>
                    @endforeach
                </table>
                @endif
                @if (session('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ session('error') }}
                </div>
                @endif
                <div class="card-body">
                    <div class="input-group mb-3 ">
                        <form action="" class="d-flex" method="POST" enctype="multipart/form-data" id="form">
                            @csrf
                            <div class="custom-file">
                                <input type="file" class="custom-file-input  @error('file') is-invalid @enderror"
                                    name="file" id="inputGroupFile02">
                                <label class="custom-file-label" for="inputGroupFile02"></label>
                                @error('file')
                                <span class="invalid-feedback text-danger" role="alert">
                                    {{ $message }}
                                </span>
                                @enderror
                            </div>
                            <div class="input-group-append">
                                <button class="input-group-text" type="submit" id="">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- /.card-header -->

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <script>
        // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    </script>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $('#form').validate({ // initialize the plugin
            rules: {
                file: {
                    required: true,
                    extension : "csv|xls|xlsx"
                }
            },

            messages: {
                file : "nhap file co dinh dang xls, xlsx, csv"
            }
        });
    });
</script>
</div>

@endsection
