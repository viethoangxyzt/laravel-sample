<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ __('link.login') }}</title>
	<base href="/backend/" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	<link rel="stylesheet" href="../../../public/backend/Awesome/css/all.css>

</head>

<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">{{ __('link.login') }}</div>
				<div class="panel-body">
					@if (session('false_login'))
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  {{ session('false_login') }}
					@endif
					@if (session('message'))
					<div class="alert alert-success alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  {{ session('message') }}
					@endif
					<form role="form" method="POST" action="{{ route('postLogin') }}">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="E-mail" name="email" type="text" autofocus="" value="{{old('email')}}">
								@error('email')
								<span class="invalid-feedback text-danger" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="{{old('password')}}" id="myInput">
								@error('password')
								<span class="invalid-feedback text-danger" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
							</div>
							<div class="checkbox">
								<label>
									<input name="showpassword" type="checkbox" onclick="myFunction()">Hiển thị mật khẩu
								</label>
							</div>
							<button type="submit" class="btn btn-primary">{{ __('link.login') }}</button>
							<a style="margin-left: 350px" href="{{ route('forgotPassword') }}" class="btn btn-warning">Quên mật khẩu</a>
							@csrf
						</fieldset>
					</form>
					<div style="background: rgb(219, 10, 10);  text-align: center; margin-top: 10px; line-height: 2.5; height: 35px;">
						<a style="color: white" href=" {{ route('social.oauth', 'google') }} ">Đăng nhập google</a>
					</div>	
					<div style="background: rgb(4, 144, 238); text-align: center; margin-top: 10px; line-height: 2.5; height: 35px;">
						<a style="color: white" href=" {{ route('social.oauth', 'facebook') }} ">Đăng nhập facebook</a>
					</div>			
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->
</body>
<script>
function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
</html>
