@extends("backend/master/master")
@section("title", "Trang chủ quản trị")
@section("main")
<div class="container">
    {{-- <h2 style="text-align: center;">Pie Chart Integration in Laravel 8</h2> --}}
    <div class="panel panel-primary">
        <div class="panel-heading">Dashboard</div>
        <div class="panel-body">
            <div id="pie-chart"></div>
        </div>
        <div class="panel-body">
            <div id="bar-chart"></div>
        </div>
    </div>
</div>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>
    $(document).ready(function() {

        $.ajax({
            url : 'http://localhost:49081/api/chart/pie-chart',
            type: "get",
            success: function(response){
                Highcharts.chart('pie-chart', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Employees in department'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                            }
                        }
                    },
                    series: [{
                    name: 'Browsers',
                        colorByPoint: true,
                        data: response
                    }]
                });
            }
        });
       
       $.ajax({
            url: 'http://localhost:49081/api/chart/col-chart',
            type: 'GET',
            success: function(data){
                // Column Chart
                Highcharts.chart('bar-chart', {
                    chart : {
                        type: 'column'
                    },
                    title: {
                        text: 'User join project'
                    },
                    xAxis: {
                        categories: data.months,
                        crosshair: true,
                        title: {
                            text: 'Months'
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Users'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key} Marks</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: data.dataBar
                }); 

            }
       });      
    });
</script>

@endsection