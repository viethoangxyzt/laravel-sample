-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `departments` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1,	'Department 1',	'Đây là department 1',	'2022-03-21 02:07:44',	'2022-03-21 02:07:44'),
(2,	'Hành chính',	'Đây là bộ phận hành chính',	'2022-03-21 02:39:24',	'2022-03-21 02:39:24'),
(4,	'department 2',	'Đây là department 2',	'2022-03-21 03:04:00',	'2022-03-21 03:58:51'),
(7,	'department 4',	'Đây là bộ phận hành chính',	'2022-03-22 04:49:25',	'2022-03-22 04:49:25'),
(8,	'department 10',	'Đây là bộ phận hành chính',	'2022-03-22 04:49:44',	'2022-03-22 04:49:44'),
(12,	'department 4',	'Đây là bộ phận hành chính',	'2022-03-31 08:00:22',	'2022-03-31 08:00:22'),
(13,	'department 90',	'Đây là department 90',	'2022-03-31 08:41:23',	'2022-03-31 08:41:23');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2022_03_21_014926_create_departments_table',	1),
(2,	'2022_03_21_015018_create_password_resets_table',	1),
(3,	'2022_03_21_015817_create_users_table',	1);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `departments_id` int unsigned NOT NULL,
  `is_manager` tinyint(1) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_work` date NOT NULL,
  `birthday` date NOT NULL,
  `status` tinyint NOT NULL,
  `first_login` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `phone`, `is_admin`, `departments_id`, `is_manager`, `image`, `start_work`, `birthday`, `status`, `first_login`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'admin',	'admin@gmail.com',	NULL,	'$2y$10$NmzGCR3B47z3CVlzUe8IWOI08UEyrRuJg1ggMYwoHP80N1PghjQha',	'0123456789',	1,	1,	1,	'avatar.jpg',	'2022-03-21',	'2000-01-01',	1,	0,	NULL,	'2022-03-21 02:00:09',	'2022-03-21 02:00:09'),
(3,	'Nguyen van c',	'nguyenvanc@gmail.com',	NULL,	'$2y$10$mCsSZhwRfKq6bhWxsAulROIIp3tDWumE7Dyg/KN.VTqEJybb0OhAm',	NULL,	0,	1,	0,	'avatar2.png',	'2022-01-01',	'2000-01-01',	1,	0,	NULL,	'2022-03-21 08:04:54',	'2022-03-21 08:04:54'),
(4,	'Nguyễn Văn A',	'nguyenvana@gmail.com',	NULL,	'$2y$10$sK4HDH22nSxaLyGPdjbmeuA9tGsZ16VMGBhYCYnWn3iBcPVMwEDVO',	NULL,	0,	1,	0,	'avatar2.png',	'2022-01-01',	'2000-01-01',	1,	0,	NULL,	'2022-03-21 08:21:13',	'2022-03-21 08:21:13'),
(5,	'Nguyễn Văn C',	'nguyanvanc@gmail.com',	NULL,	'$2y$10$gcKDLuv6Jb2XoH8n17aSb.UJzysgc/J9Dv0yWPHLD15LaRRBatIhi',	'0123456789',	0,	4,	0,	'avatar2.png',	'2022-01-01',	'2000-01-01',	1,	0,	NULL,	'2022-03-21 08:24:59',	'2022-03-22 04:17:08'),
(7,	'Cao Việt Hoàng',	'caoviethoang@gmail.com',	NULL,	'$2y$10$hZ/YuCyWRmsJWNfuLv28IunjAxKr6uTtd/O/kunSPkU39MOYnLI6e',	'0358085833',	1,	8,	1,	'avatar2.png',	'2022-01-01',	'2000-01-01',	0,	0,	NULL,	'2022-03-22 07:34:01',	'2022-03-22 07:34:01'),
(8,	'Cao Việt Hoàng 123',	'caoviethoang123455@gmail.com',	NULL,	'$2y$10$5U4pqcmtvCFyDUTeIQHHauxHmgT653G.Ox33i0sT3RMX6ZTSo8eXm',	'0909090909',	1,	1,	0,	'avatar2.png',	'2021-12-09',	'1997-12-02',	1,	0,	NULL,	'2022-03-23 01:08:48',	'2022-03-28 07:59:55'),
(9,	'nguyen nguyen nguyen',	'nguyen@gmail.com',	NULL,	'$2y$10$5CthQSdxQSQ0YA6.p57tAu3V3UY84wDeb/xQdwzSKj/J46bhX5592',	'0123456789',	0,	1,	0,	'images.jpg',	'2022-01-02',	'2004-09-09',	0,	0,	NULL,	'2022-03-28 08:55:55',	'2022-03-28 08:55:55'),
(12,	'Tran Van Bom',	'caoviethoang123@gmail.com',	NULL,	'$2y$10$3n1DLDtmt5ppAJDB7CbZTuBgmi0oRVws4EF1eK6MeaA.3l2xuB7xi',	'0123456789',	0,	1,	0,	'images.jpg',	'2019-01-09',	'1998-05-06',	0,	0,	NULL,	'2022-03-31 02:12:34',	'2022-03-31 02:12:34'),
(14,	'Tran Van Bom',	'caoviethoang987@gmail.com',	NULL,	'$2y$10$dxMi16GB7uXpNFUkdiIboeiUzuapNEdHBRvQxq5.gkradSzUhu.FO',	'0123456789',	0,	1,	0,	'avatar2.png',	'2019-01-09',	'1998-05-06',	0,	0,	NULL,	'2022-03-31 02:14:40',	'2022-03-31 02:14:40'),
(20,	'Cao Việt Hoàng',	'caoviethoang12345678@gmail.com',	NULL,	'$2y$10$3LdUZRYlsrIHXYoVWnyKB.9BhOzALHXEeef8UpYUqvpaYBPuqM9zK',	'0958085833',	0,	1,	0,	'avatar2.png',	'2020-05-07',	'1999-01-03',	1,	0,	NULL,	'2022-03-31 02:54:32',	'2022-03-31 02:54:32'),
(22,	'Cao Việt Hoàng',	'caoviethoang6996@gmail.com',	NULL,	'$2y$10$6VTZxl2X20Y8uVv3K.dIFe75FFl08j1L.dL.VT8gtBNxeBSs/kYRK',	'0358085833',	0,	1,	0,	'images.jpg',	'2022-01-01',	'2000-01-01',	1,	1,	NULL,	'2022-03-31 03:58:09',	'2022-03-31 07:07:43'),
(24,	'dang van van',	'caoviethoang2906@gmail.com',	NULL,	'$2y$10$PbTGT3VOeAnbMBu8YdCci.J.7eqvcKeSxkPx/KKd1ABJmgiC4C07O',	'0987654321',	0,	1,	1,	'avatar2.png',	'2021-04-05',	'2003-01-02',	1,	0,	NULL,	'2022-03-31 07:45:05',	'2022-03-31 07:45:05'),
(25,	'dang van van',	'cao@gmail.com',	NULL,	'$2y$10$DvqrQis1OMiJwJWZrkjmCO0V3QCsUIrqen8IT9DPKJfeoJR6uoQsC',	'0987654321',	0,	1,	1,	'avatar2.png',	'2021-04-05',	'2003-01-02',	1,	0,	NULL,	'2022-03-31 07:45:41',	'2022-03-31 07:45:41');

-- 2022-03-31 09:13:16
