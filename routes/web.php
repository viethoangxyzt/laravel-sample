<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\SocialAuthController;
use App\Http\Controllers\UserController;
use App\Models\Department;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(["prefix" => "/login"], function () {
    Route::get("/", [LoginController::class, "login"])->name('login');
    Route::post("/", [LoginController::class, "postLogin"])->name('postLogin');
    Route::get("/logout", [LoginController::class, "logout"])->name('logout');
});

Route::get('/forgotPassword', [ForgotPasswordController::class, "submitMailForm"])->name('forgotPassword');
Route::post('/forgotPassword', [ForgotPasswordController::class, "submitMail"])->name('submitMail');
Route::get('change-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('change-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');


Route::middleware('auth')->group(function () {
    Route::group(["prefix" => "admin"], function () {
        Route::get("/", [AdminController::class, "index"]);
        Route::get("/user", [UserController::class, "list"])->name('user.index');
        Route::group(["prefix" => "user", "middleware" => "checkadmin"], function () {
            Route::get("/create", [UserController::class, "create"])->name('user.create');
            Route::post("/store", [UserController::class, "store"])->name('user.store');
            Route::get("/edit/{id}", [UserController::class, "edit"])->name('user.edit');
            Route::post("/update/{id}", [UserController::class, "update"]);
            Route::get("/delete/{id}", [UserController::class, "delete"])->name('user.delete');
            Route::any("/sort", [UserController::class, "sort"])->name('user.sort');
            Route::get("/search", [UserController::class, "search"])->name('user.search');
            Route::get("/export", [Usercontroller::class, "exportUsers"])->name('user.export');
            Route::get('/createExcel', [UserController::class, 'importExcel'])->name('user.createExcel');
            Route::post('/createExcel', [UserController::class, 'saveImportExcel'])->name('user.saveImportExcel');
        });

        Route::group(["prefix" => "department", "middleware" => "checkadmin"], function () {
            Route::get("/", [DepartmentController::class, "list"])->name('department.index');
            Route::get("/create", [DepartmentController::class, "create"])->name('department.create');
            Route::post("/store", [DepartmentController::class, "store"])->name('department.store');
            Route::get("/edit/{id}", [DepartmentController::class, "edit"])->name('department.edit');
            Route::post("/update/{id}", [DepartmentController::class, "update"])->name('department.update');
            Route::get("/delete/{id}", [DepartmentController::class, "delete"])->name('department.delete');
            Route::get("/addUser", [DepartmentController::class, "formAddUser"])->name('department.formAddUser');
            Route::post("/addUser", [DepartmentController::class, "addUser"])->name('department.addUser');
        });

        Route::group(["prefix" => "project", "middleware" => "checkadmin"], function () {
            Route::get("/", [ProjectController::class, "index"])->name('project.index');
            Route::get("/create", [ProjectController::class, "create"])->name('project.create');
            Route::post("/store", [ProjectController::class, "store"])->name('project.store');
            Route::get("/edit/{id}", [ProjectController::class, "edit"])->name('project.edit');
            Route::post("/update/{id}", [ProjectController::class, "update"])->name('project.update');
            Route::get("/delete/{id}", [ProjectController::class, "delete"])->name('project.delete');
            Route::get("/detail/{id}", [ProjectController::class, "detail"])->name('project.detail');
            Route::get("/users/{id}", [ProjectController::class, "formAddUser"])->name('project.user.formAdd');
            Route::post("/users/{id}", [ProjectController::class, "addUser"])->name('project.user.add');
            Route::get('users/{projectId}/edit/{userId}', [ProjectController::class, "editUser"])->name('project.user.edit');
            Route::post('users/{projectId}/update/{userId}', [ProjectController::class, "updateUser"])->name('project.user.update');
            Route::get('users/{projectId}/delete/{userId}', [ProjectController::class, "deleteUser"])->name('project.user.delete');
        });
        Route::get('/dashboard', [AdminController::class, "handleChart"])->name('dashboard');
    });

    Route::group(["prefix" => "profile/{id}"], function () {
        Route::get("/", [UserController::class, "detail"])->name('profile');
        Route::get("/update", [UserController::class, "updateProfile"])->name('profile.update');
        Route::post("/update", [UserController::class, "storeProfile"])->name('profile.store');
        Route::get("/pdf", [UserController::class, "exportCV"])->name('profile.cv');
        Route::get("/listproject", [ProjectController::class, "listProjectUser"])->name('profile.listproject');
        Route::get("/listprojectDepartment", [ProjectController::class, "listProjectDepartment"])->name('manager.listproject');
    });

    Route::group(["prefix" => "notifications"], function () {
        Route::get("/", [NotificationController::class, "index"])->name('notifications.index');
        Route::get("/create", [NotificationController::class, "create"])->name('notifications.add');
        Route::post("/store", [NotificationController::class, "store"])->name('notifications.store');
        Route::get("/edit/{id}", [NotificationController::class, "edit"])->name('notifications.edit');
        Route::post("/update/{id}", [NotificationController::class, "update"])->name('notifications.update');
        Route::get("/delete/{id}", [NotificationController::class, "delete"])->name('notifications.delete');
        Route::get("/markAsRead/{id}", [NotificationController::class, "markAsRead"])->name('notifications.markAsRead');
    });

    Route::group(["prefix" => "/change-password"], function () {
        Route::get("/form", [UserController::class, "formChangePassword"])->name('formChange');
        Route::post("/store", [UserController::class, "changePassword"])->name('changePassword');
    });
    Route::get("reset-password/{id}", [UserController::class, "resetPassword"])->name('reset');
});


Route::get('login/{social}', [SocialAuthController::class, "redirectToProvider"])->name('social.oauth');
Route::get('{social}/callback', [SocialAuthController::class, "handleProviderCallback"])->name('social.callback');
Route::get('register/{id}', [UserController::class, "formRegister"])->name('formRegister');
Route::post('register/{id}', [UserController::class, "register"])->name('register');


Route::get('departments', function () {
    $users = User::all();
    return view('backend.departments', compact('users'));
});

Route::group(["prefix" => "/users"], function () {
    Route::get('/', function () {
        $departments = Department::all();
        return view('backend.users_api.users', compact('departments'));
    })->name('users.api.index');

    Route::get('/create', [UserController::class, "createByApi"])->name('user.api.add');
    Route::get('/edit/{id}', [UserController::class, "editByApi"])->name('user.api.edit');
});

Route::get("/test", [NotificationController::class, "test"]);
