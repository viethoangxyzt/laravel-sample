<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Api\AuthController@login');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
    });
});

Route::group(["prefix" => "users"], function () {
    Route::get('/', 'Api\UserController@index')->name('users.index');
    Route::get('/list', 'Api\UserController@list')->name('api.users.list');
    Route::get('/{id}', 'Api\UserController@show')->name('users.show');
    Route::post('', 'Api\UserController@store')->name('users.store');
    Route::put('/{id}', 'Api\UserController@update')->name('users.update');
    Route::delete('/{id}', 'Api\UserController@destroy')->name('users.destroy');
    Route::post('/search', 'Api\UserController@search')->name('api.users.search');
    Route::get('/department/{id}', 'Api\UserController@usersInDepartment')->name('department.user');
    Route::get('/status/{status}', 'Api\UserController@getUserByStatus')->name('user.status');
    Route::get('department/{id}/status/{status}/sort/{sort}', 'Api\UserController@sortUser')->name('user.sort');
});

Route::group(["prefix" => "notifications"], function () {
    Route::get('/user-notification/{id}', 'Api\NotificationController@getListUserByNotification')->name('api.notification.user');
});

Route::group(["prefix" => "chart"], function () {
    Route::get('/pie-chart', 'Api\UserController@pieChart')->name('chart.pie');
    Route::get('/col-chart', 'Api\UserController@colChart')->name('chart.col');
});

Route::post('/processImage', 'Api\UserController@processImage')->name('processImage');

// Department
Route::get('departments', 'Api\DepartmentController@index')->name('departments.index');

Route::get('departments/{id}', 'Api\DepartmentController@show')->name('departments.show');

Route::post('departments', 'Api\DepartmentController@store')->name('departments.store');

Route::put('departments/{id}', 'Api\DepartmentController@update')->name('departments.update');

Route::delete('departments/{id}', 'Api\DepartmentController@destroy')->name('departments.destroy');
